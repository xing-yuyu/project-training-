const multer = require('koa-multer')

let storage = multer.diskStorage({
   //文件保存路径 这个路由是以项目文件夹  
  destination: function (req, file, cb) {
    cb(null, '../../home/nvidia/images/public/upload/')
   },
  //修改文件名称
  filename: function (q, file, cb) {
    let fileFormat = (file.originalname).split(".");  //以点分割成数组，数组的最后一项就是后缀名
    cb(null, 'pic'+Date.now() + "." + fileFormat[fileFormat.length - 1]);//加入时间戳
  }
})

let upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024*1024/2 // 限制512KB  
  }
});

module.exports = { upload }



