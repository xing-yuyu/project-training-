#!/usr/bin/env node

const mysql = require('mysql2/promise'),
      log   = console.log,
      Koa   = require('koa'),
      app   = new Koa(),
      Router = require('koa-router'),
      router = new Router(),
      fs    = require('fs'),
      path  = require('path'),
      cors  = require('koa2-cors'),
      http  = require('http'),
      https = require('https');

let con = mysql.createPool({
    host:'154.8.156.240',
    port:'2020',
    user:'root',
    password:'Ljj0222.',
    database:'tuijian'
});
//添加评论
async function insertcomments(share_id,user_id,date,text){
    const sql =`insert into comments_table(share_id,user_id,date,text,zan) values('${share_id}','${user_id}','${date}','${text}',0)`;
    let [rows] = await con.execute(sql);
    return rows;
}
//通过share_id查询对应板块评论
async function getid_comments(share_id){
    const sql = `select comments_table.id,share_id,user_name,user_img,date,text,zan from comments_table left join user_table on user_table.id = comments_table.user_id where comments_table.share_id = '${share_id}' `;
    let [rows] = await con.execute(sql);
    return rows;
}
//通过查询state
async function get_state(user_id,comments_id){
  const sql = `select * from comments_state_table where user_id ='${user_id}' and comments_id ='${comments_id}'`; 
  let [rows] = await con.execute(sql);
  return rows;
}
//添加状态
async function insert_state(user_id,comments_id,state){
  const sql = `insert into comments_state_table(user_id,comments_id,state) values('${user_id}','${comments_id}','${state}')`;
  let [rows] = await con.execute(sql);
  return rows;
}
//更改赞数量
async function zannum(comments_id,zan){
  const sql = `update comments_table set zan = ${zan} where id = ${comments_id}`;
  let [rows] = await con.execute(sql);
  return rows;
}
//通过id查询数据
async function getid_zan(id){
    const sql=`select * from comments_table where id = '${id}'`;
    let [rows] = await con.execute(sql);
    return rows;
}
//点赞
async function updatezan(user_id,comments_id,state){
    const sql=`update comments_state_table set state='${state}' where user_id ='${user_id}' and comments_id = '${comments_id}'`;
    let [rows] = await con.execute(sql);
    return rows;
}

(async function(){
    //log(await getAreas());
})();

module.exports.insertcomments = insertcomments;
module.exports.getid_comments = getid_comments;
module.exports.get_state = get_state;
module.exports.insert_state = insert_state;
module.exports.zannum =zannum;
module.exports.getid_zan = getid_zan;
module.exports.updatezan = updatezan;
