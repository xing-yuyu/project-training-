#!/usr/bin/env node

const mysql = require('mysql2/promise'),
      log   = console.log,
      Koa   = require('koa'),
      app   = new Koa(),
      Router = require('koa-router'),
      router = new Router(),
      fs    = require('fs'),
      path  = require('path'),
      cors  = require('koa2-cors'),
      http  = require('http'),
      https = require('https');

let con = mysql.createPool({
    host:'154.8.156.240',
    port:'2020',
    user:'root',
    password:'Ljj0222.',
    database:'tuijian'
});
//添加收藏状态
async function insertuser_collect(user_id,share_id){
    const sql = `insert into concern_collect_table(user_id,share_id,state) values('${user_id}','${share_id}',1)`;
    let [rows] = await con.execute(sql);
    return rows;
}

//用户更改关注收藏状态
async function updateuser_collect(user_id,share_id,state){
    const sql =`update concern_collect_table set state = '${state}' where user_id = '${user_id}' and share_id = '${share_id}'`;
    let [rows] = await con.execute(sql);
    return rows;
}
//展示页面状态
async function getcollect(){
    const sql =`select main_table.share_id,concern_collect_table.user_id,concern_collect_table.state from main_table left join concern_collect_table on concern_collect_table.share_id = main_table.share_id where judge !=2 order by share_id desc  `;
    let [rows] = await con.execute(sql);
    return rows;
}
//通过user_id查询关注
async function getuser_collect(user_id){
    const sql =`select * from concern_collect_table left join main_table on main_table.share_id = concern_collect_table.share_id where concern_collect_table.user_id = '${user_id}' and state = 1 and judge !=2`;
    let [rows] = await con.execute(sql);
    return rows;
}

async function getid_collect(id){
    const sql = `select share_id,title from main_table where share_id='${id}' and judge != 2`;
    let [rows] = await con.execute(sql);
    return rows;
}

async function getid_img(id){
    const sql = `select img1,size from img_table where id = '${id}'`;
    let [rows] = await con.execute(sql);
    return rows;
}

async function getsb(user_id,share_id){
  const sql = `select * from concern_collect_table where share_id = '${share_id}' and user_id = '${user_id}'`;
  let [rows] = await con.execute(sql);
  return rows;
}

(async function(){
    //log(await getAreas());
})();

module.exports.insertuser_collect = insertuser_collect;
module.exports.updateuser_collect = updateuser_collect;
module.exports.getuser_collect = getuser_collect;
module.exports.getid_collect = getid_collect;
module.exports.getcollect = getcollect;
module.exports.getsb = getsb;
module.exports.getid_img = getid_img;

