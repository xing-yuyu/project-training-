#!/usr/bin/env node

const mysql = require('mysql2/promise'),
      log   = console.log,
      Koa   = require('koa'),
      app   = new Koa(),
      Router = require('koa-router'),
      router = new Router(),
      fs    = require('fs'),
      path  = require('path'),
      cors  = require('koa2-cors'),
      http  = require('http'),
      https = require('https');

let con = mysql.createPool({
    host:'154.8.156.240',
    port:'2020',
    user:'root',
    password:'Ljj0222.',
    database:'tuijian'
});
//添加用户状态
async function insertuser_dav(user_id,dav_id){
    const sql = `insert into concern_dav_table(user_id,dav_id,state) values('${user_id}','${dav_id}',1)`;
    let [rows] = await con.execute(sql);
    return rows;
}

//用户更改关注dav状态
async function updateuser_dav(user_id,dav_id,state){
    const sql =`update concern_dav_table set state = '${state}' where user_id = '${user_id}' and dav_id = '${dav_id}'`;
    let [rows] = await con.execute(sql);
    return rows;
}

//通过dav_id查询关注人数
async function getdav_num(dav_id,state){
    const sql = `select * from concern_dav_table where dav_id ='${dav_id}' and state = '${state}'`;
    let [rows] = await con.execute(sql);
    return rows;
}

//通过user_id查询关注dav
async function getuser_dav(user_id){
    const sql =`select * from concern_dav_table where user_id = '${user_id}' and state =1`;
    let [rows] = await con.execute(sql);
    return rows;
}

async function getid_dav(id){
    const sql = `select * from user_table where id='${id}'`;
    let [rows] = await con.execute(sql);
    return rows;
}

async function getuser_fensi(user_id){
    const sql =`select * from concern_dav_table where dav_id = '${user_id}'`;
    let [rows] = await con.execute(sql);
    return rows;
}

async function getid_fensi(id){
    const sql = `select * from user_table where id='${id}'`;
    let [rows] = await con.execute(sql);
    return rows;
}

(async function(){
    //log(await getAreas());
})();

module.exports.insertuser_dav = insertuser_dav;
module.exports.updateuser_dav = updateuser_dav;
module.exports.getuser_dav = getuser_dav;
module.exports.getdav_num = getdav_num;
module.exports.getid_dav = getid_dav;
module.exports.getuser_fensi = getuser_fensi
module.exports.getid_fensi = getid_fensi;


