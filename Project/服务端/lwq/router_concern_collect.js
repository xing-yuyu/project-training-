const mysql = require('mysql2/promise'),
      log   = console.log,
      Koa   = require('koa'),
      app   = new Koa(),
      Router = require('koa-router'),
      router = new Router(),
      fs    = require('fs'),
      path  = require('path'),
      cors  = require('koa2-cors'),
      http  = require('http'),
      https = require('https'),
      body = require('koa-bodyparser'),
      hq = require('./concern_collect.js');

const options = {
    key:fs.readFileSync(path.join(__dirname,'./fourgoldhua.cn.key')),
    cert:fs.readFileSync(path.join(__dirname,'./fourgoldhua.cn.crt'))
}
app.use(body());
app.use(cors({
    origin: function (ctx) {
        if (ctx.url === '/test') {
          return "*"; // 允许来自所有域名请求
        }
        return 'http://localhost:8080'; // 这样就能只允许 http://localhost:8080 这个域名的请求了 
    },
    exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
    maxAge: 5,
    credentials: true,
    allowMethods: ['GET', 'POST', 'DELETE'],
    allowHeaders: ['Content-Type', 'Authorization', 'Accept'],
}))
//展示页面
/*
router.get('/concern/state',async (ctx)=>{
  let {user_id} = ctx.request.query;
  let arr = await hq.getcollect();
  //log(arr);
  for(let i = 0;i < arr.length-1 ;i++){
    if(arr[i].share_id == arr[i+1].share_id){
      if(arr[i].user_id == user_id) arr.splice(i+1,1);
      else arr.splice(i,1);
      i--;
    }
  }
  //log(arr);
  for(let i =0; i< arr.length;i++){
    if(arr[i].state==null||arr[i].user_id!=user_id) arr[i].state=2;
  }
  log(arr);
  ctx.body = JSON.stringify(arr);
})
*/
//收藏状态
router.get('/concern/state',async (ctx)=>{
  let {open_id,share_id} = ctx.request.query;
  let arr = await hq.getsb(open_id,share_id);
  if(arr.length == 1&& arr[0].state=='1'){
    ctx.body = JSON.stringify('1')
  }else if(arr.length == 0){
    ctx.body = JSON.stringify('2') 
  }else {
    ctx.body = JSON.stringify('0')
  }
})
//点击收藏
router.get('/concern/collect',async (ctx)=>{
  let {user_id,collect_id,state} = ctx.request.query;
    log(state);
  let state1;
  let brr=[];
  if(state !=null){
  if(state == 2){
    brr = await hq.getsb(user_id,collect_id);
    if(brr.length==0){
      await hq.insertuser_collect(user_id,collect_id);
      log('创建'+collect_id);
    }
    state1=1;
  }
  else if(state == 1) {
    await hq.updateuser_collect(user_id,collect_id,0);
    state1=0;
  }
  else if(state == 0){
    await hq.updateuser_collect(user_id,collect_id,1);
    state1=1;
  }
  let arr1=[];
  arr1.push({
    "state":state1
  })
  log(arr1);
  ctx.body = JSON.stringify(arr1);
  }
  else ctx.body = JSON.string('别传啦');
})

//查询用户收藏
router.get('/concern/user_collect',async (ctx)=>{
  let {user_id} = ctx.request.query;
  let arr = await hq.getuser_collect(user_id);
  log(arr);
  let crr=[];
  for(let i =0 ;i<arr.length;i++){
     
      brr = await hq.getid_collect(arr[i].share_id);
      //log(brr);
      drr = await hq.getid_img(arr[i].share_id);
      //log(drr);
      crr.push({
        "share_id":brr[0].share_id,
        "title":brr[0].title,
        "img1":drr[0].img1,
        "size":drr[0].size
      })
  }
  log(crr);
  ctx.body = JSON.stringify(crr);
})

process.on('unhandledRejection',(reason,p) => {
    console.log('Unhandled Rejection at:Promise',p,'reason:',reason);
});

app.use(router.routes())
   .use(router.allowedMethods());

https.createServer(options,app.callback()).listen(1517);
 
