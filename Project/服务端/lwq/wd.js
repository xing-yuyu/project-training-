#!/usr/bin/env node

const mysql = require('mysql2/promise'),
      log   = console.log,
      Koa   = require('koa'),
      app   = new Koa(),
      Router = require('koa-router'),
      router = new Router(),
      fs    = require('fs'),
      path  = require('path'),
      cors  = require('koa2-cors'),
      http  = require('http'),
      https = require('https');

let con = mysql.createPool({
    host:'154.8.156.240',
    port:'2020',
    user:'root',
    password:'Ljj0222.',
    database:'tuijian'
});

//用户更改关注dav状态
async function updateuser_state(user_id,dav_id,state){
    const sql =`update concern_dav_table set state = '${state}' where user_id = '${user_id}' and dav_id = '${dav_id}'`;
    let [rows] = await con.execute(sql);
    return rows;
}
//用户关注量
async function selm(user_id){
  const sql =`select * from concern_dav_table where user_id = '${user_id}' and state = 1`;
  let [rows] = await con.execute(sql);
  return rows;
}

async function selfm(user_id){
  const sql =`select * from concern_dav_table where dav_id = '${user_id}' and state = 1`;
  let [rows] = await con.execute(sql);
  return rows;
}

async function self(user_id,dav_id){
  const sql =`select * from concern_dav_table where dav_id = '${user_id}' and user_id = '${dav_id}'`;
  let [rows] = await con.execute(sql);
  return rows;
}

async function getuser(id){
  const sql = `select  *  from user_table where id = '${id}'`; 
  let [rows] = await con.execute(sql);
  return rows;
}

async function sel(user_id,dav_id){
  const sql =`select * from concern_dav_table where user_id = '${user_id}'and dav_id = '${dav_id}'`;
  let [rows] = await con.execute(sql);
  return rows;
}

async function insertuser_dav(user_id,dav_id){
    const sql = `insert into concern_dav_table(user_id,dav_id,state) values('${user_id}','${dav_id}',1)`;
    let [rows] = await con.execute(sql);
    return rows;
}
async function updateuser_dav(user_id,dav_id,state){
    const sql =`update concern_dav_table set state = '${state}' where user_id = '${user_id}' and dav_id = '${dav_id}'`;
    let [rows] = await con.execute(sql);
    return rows;
}

async function selss(name){
    const sql =`select user_table.id,user_id,user_name,user_img,state from concern_dav_table left join user_table on concern_dav_table.dav_id = user_table.id where user_table.user_name like '%${name}%' and state = 1`;
    let [rows] = await con.execute(sql);
    return rows;
}

async function getxx(user_id){
    const sql =`select * from main_table where user_id = '${user_id}' and judge != 1 order by share_id desc`;
    let [rows] = await con.execute(sql);
    return rows;
}

async function delxx(share_id){
  const sql = `delete from main_table where share_id = '${share_id}'and judge = 2 `;
  //log(sql);  
  let [rows] = await con.execute(sql);
  return rows;
}

async function delxxi(id){
  const sql = `delete from img_table where id='${id}'`;
  let [rows] = await con.execute(sql);
  return rows;
}

async function delxxc(id){
  const sql = `delete from concern_collect_table where share_id = '${id}'`;
  let [rows] = await con.execute(sql);
  return rows;
}

async function getxxh(user_id){
    const sql =`select * from main_table where user_id = '${user_id}' and judge = 2`;
    let [rows] = await con.execute(sql);
    return rows;
}

(async function(){
    //log(await getAreas());
})();

module.exports.updateuser_state = updateuser_state;
module.exports.sel = sel;
module.exports.selm = selm;
module.exports.selfm = selfm;
module.exports.self = self;
module.exports.getuser = getuser ;
module.exports.insertuser_dav = insertuser_dav;
module.exports.updateuser_dav = updateuser_dav;
module.exports.selss = selss;
module.exports.getxx = getxx;
module.exports.delxx = delxx;
module.exports.delxxi = delxxi;
module.exports.delxxc = delxxc;
module.exports.getxxh = getxxh;
