#!/usr/bin/env node

const mysql = require('mysql2/promise'),
      log   = console.log,
      Koa   = require('koa'),
      app   = new Koa(),
      Router = require('koa-router'),
      router = new Router(),
      fs    = require('fs'),
      path  = require('path'),
      cors  = require('koa2-cors'),
      http  = require('http'),
      https = require('https');

let con = mysql.createPool({
    host:'154.8.156.240',
    port:'2020',
    user:'root',
    password:'Ljj0222.',
    database:'tuijian'
});
//添加分享属性
async function insert_main(open_id,shop_id, title, text, date, addr, tags, food, top, judge, jwd){
  const sql = `insert into main_table(user_id,shop_id, title, text, date, addr, tags, food, top, judge, jwd) 
        values('${open_id}', '${shop_id}', '${title}', '${text}', '${date}', '${addr}', '${tags}', 
        '${food}', '${top}', '${judge}', '${jwd}')`; 
  let [rows] = await con.execute(sql);
  return rows;
}
//搜索main
async function get_main(date){
  const sql = `select * from main_table where date = '${date}'`;
  let [rows] = await con.execute(sql);
  return rows;
}
//添加图片1
async function insert_img(id,img1,size){
  const sql = `insert into img_table(id,img1,size) values('${id}','${img1}','${size}')`; 
  log(sql)
  let [rows] = await con.execute(sql);
  return rows;
}
//添加图片2
async function update_img(id,imgn,img){
  const sql = `update img_table set ${imgn} = '${img}' where id = ${id}`;
  let [rows] = await con.execute(sql);
  return rows;
}
//查询图片
async function get_img(id){
  const sql= `select * from img_table where id = '${id}'`;
  let [rows] = await con.execute(sql);
  return rows;
}
//查询分数
async function get_top(shop_id){
  const sql = `select * from Merchants_settled_table where id = ${shop_id}`;
  let [rows] = await con.execute(sql);
  return rows;
}
//修改分数
async function update_top(shop_id,top,num){
  const sql = `update Merchants_settled_table set score = '${top}',num = '${num}' where id = ${shop_id}`
}
(async function(){
    //log(await getAreas());
})();

module.exports = {
  insert_main,
  insert_img,
  get_main,
  update_img,
  update_top,
  get_img,
  get_top,
}

