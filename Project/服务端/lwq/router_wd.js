const mysql = require('mysql2/promise'),
      log   = console.log,
      Koa   = require('koa'),
      app   = new Koa(),
      Router = require('koa-router'),
      router = new Router(),
      fs    = require('fs'),
      path  = require('path'),
      cors  = require('koa2-cors'),
      http  = require('http'),
      https = require('https'),
      body = require('koa-bodyparser'),
      hq = require('./wd.js');

const options = {
    key:fs.readFileSync(path.join(__dirname,'./fourgoldhua.cn.key')),
    cert:fs.readFileSync(path.join(__dirname,'./fourgoldhua.cn.crt'))
}
app.use(body());
app.use(cors({
    origin: function (ctx) {
        if (ctx.url === '/test') {
          return "*"; // 允许来自所有域名请求
        }
        return 'http://localhost:8080'; // 这样就能只允许 http://localhost:8080 这个域名的请求了 
    },
    exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
    maxAge: 5,
    credentials: true,
    allowMethods: ['GET', 'POST', 'DELETE'],
    allowHeaders: ['Content-Type', 'Authorization', 'Accept'],
}))
//模糊查询
router.get('/wd/ss',async (ctx)=>{
  let {user_id,name} = ctx.request.query;
  let arr = await hq.selss(name);
  let brr = [];
  let crr = [];
  for(let i=0;i<arr.length;i++){
    if(arr[i].user_id == user_id){
      crr = await hq.sel(arr[i].id,arr[i].user_id);
      if(crr.length==1) state1='10';
      else state1= arr[i].state;
      brr.push({
         "id":arr[i].id,
         "user_name":arr[i].user_name,
         "user_img":arr[i].user_img,
         "state":state1
      })
    } 
  }
  log(brr);
  ctx.body = JSON.stringify(brr);
})
//我的关注
router.get('/wd/dav',async (ctx)=>{
  let {user_id} = ctx.request.query;
  let arr = await hq.selm(user_id);
  let crr = [];
  let state1;
  for(let i = 0 ; i < arr.length ; i++){
    let brr = await hq.sel(arr[i].dav_id,arr[i].user_id);
    if(brr.length==1) state1='10';
    else state1=arr[i].state;
    let drr = await hq.getuser(arr[i].dav_id);
    crr.push({
      "id":drr[0].id,
      "user_name":drr[0].user_name,
      "user_img":drr[0].user_img,
      "state":state1
    })
  }
  log(crr);
  ctx.body = JSON.stringify(crr);
})
//我的粉丝
router.get('/wd/fensi',async (ctx)=>{
  let {user_id} = ctx.request.query;
  let arr = await hq.selfm(user_id);
  let crr = [];
  log(arr)
  for(let i = 0 ; i < arr.length ; i++){
      let brr = await hq.sel(arr[i].dav_id,arr[i].user_id);
      log(brr)
      if(brr.length==1&&brr[0].state == 1) state1='10';
      else if(brr.length==1&&brr[0].state == 0)state1='0';
      else if(brr.length==0) state1 = '0';
      let drr = await hq.getuser(arr[i].user_id);
      log(drr)
      crr.push({
            "id":drr[0].id,
            "user_name":drr[0].user_name,
            "user_img":drr[0].user_img,
            "state":state1
            })
      }
    log(crr);
  ctx.body = JSON.stringify(crr);
})

//取消关注
router.get('/wd/quguan',async (ctx)=>{
  let {user_id,dav_id,state} = ctx.request.query;
  let arr = await hq.sel(dav_id,user_id);
  if(state ==10||state == 1){
    state1 ='0';
    state2 ='0';
  }
  else if(state==0&&arr.length==0){
    state1 ='1'; 
    state2 ='0';
  }
  else if(state==0&&arr[0].state==0) {
    state1 = '1';
    state2 = '0';
  }
  else if(state==0&&arr[0].state==1){
    state1 = '10';
    state2 = '0';
   }
  await hq.updateuser_state(user_id,dav_id,state2);
  let brr= [];
  brr.push({
    "user_id":user_id,
    "dav_id":dav_id,
    "state":state1
  })
  log(brr);
  ctx.body = JSON.stringify(brr);
  
})
//互关
router.get('/wd/huguan',async (ctx)=>{
  let {user_id,fensi_id,state} = ctx.request.query;
  let arr = await hq.sel(user_id,fensi_id);
  let brr = [];
  log(arr);
  log(user_id,fensi_id,state);
  if(arr.length==0){
    await hq.insertuser_dav(user_id,fensi_id);
      brr.push({
        "user_id":user_id,
        "fensi_id":fensi_id,
        "state": '1'
      })
  }
  else if(arr.length==1){
    if(state==0){
      state1='1';
      state2='10';
    }
    else if(state==10){
      state1='0';
      state2='0';
    }
    await hq.updateuser_dav(user_id,fensi_id,state1); 
      brr.push({
          "user_id":user_id,
          "fensi_id":fensi_id,
          "state":state2
      })
  }
  log(brr);
  ctx.body = JSON.stringify(brr);
})
//获取未被审核通过的信息
router.get('/wd/xx',async (ctx)=>{
  let {user_id} =ctx.request.query;
  let arr = await hq.getxx(user_id);
  log(arr);
  ctx.body = JSON.stringify(arr);
})
//删除未被审核通过
router.get('/wd/xx/del',async (ctx)=>{
  let {share_id,user_id} = ctx.request.query;
  await hq.delxx(share_id);
  await hq.delxxi(share_id);
  await hq.delxxc(share_id);
  let arr = await hq.getxx(user_id);
  //log(arr);
  ctx.body = JSON.stringify(arr);
})
//红点
router.get('/wd/xx/hong',async (ctx)=>{
  let {user_id} =ctx.request.query;
  log(user_id);
  let arr = await hq.getxxh(user_id);
  log(arr);
  let brr =[]; 
  if(arr.length==0){
    brr.push({
      'hong':0
    })
  }else{
    brr.push({
      'hong':1
    })
  }
  ctx.body = JSON.stringify(brr);
})

process.on('unhandledRejection',(reason,p) => {
    console.log('Unhandled Rejection at:Promise',p,'reason:',reason);
});


app.use(router.routes())
   .use(router.allowedMethods());
//app.listen(2227);


https.createServer(options,app.callback()).listen(2002);

