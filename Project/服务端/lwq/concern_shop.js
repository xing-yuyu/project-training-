#!/usr/bin/env node

const mysql = require('mysql2/promise'),
      log   = console.log,
      Koa   = require('koa'),
      app   = new Koa(),
      Router = require('koa-router'),
      router = new Router(),
      fs    = require('fs'),
      path  = require('path'),
      cors  = require('koa2-cors'),
      http  = require('http'),
      https = require('https');

let con = mysql.createPool({
    host:'154.8.156.240',
    port:'2020',
    user:'root',
    password:'Ljj0222.',
    database:'tuijian'
});
//添加用户状态
async function insertuser_shop(user_id,shop_id){
    const sql = `insert into concern_shop_table(user_id,shop_id,state) values('${user_id}','${shop_id}',1)`;
    let [rows] = await con.execute(sql);
    return rows;
}

//用户更改关注店家状态
async function updateuser_shop(user_id,shop_id,state){
    const sql =`update concern_shop_table set state = '${state}' where user_id = '${user_id}' and shop_id = '${shop_id}'`;
    let [rows] = await con.execute(sql);
    return rows;
}

//通过shop_id查询关注人数
async function getshop_num(shop_id,state){
    const sql = `select * from concern_shop_table where shop_id ='${shop_id}' and state = '${state}'`;
    let [rows] = await con.execute(sql);
    return rows;
}

//通过user_id查询关注商家
async function getuser_shop(user_id,state){
    const sql =`select * from concern_shop_table where user_id = '${user_id}' and state='${state}'`;
    let [rows] = await con.execute(sql);
    return rows;
}
//通过id查询shop数据
async function getid_shop(id){
    const sql = `select id,shop_name,shop_jwd,shop_img,shop_label,shop_starcount,shop_count,shop_distribution1,shop_distribution2,shop_reduction1,shop_reduction2,shop_reduction3 from shop_table where id='${id}'`;
    let [rows] = await con.execute(sql);
    return rows;
}

(async function(){
    //log(await getAreas());
})();

module.exports.insertuser_shop = insertuser_shop;
module.exports.updateuser_shop = updateuser_shop;
module.exports.getuser_shop = getuser_shop;
module.exports.getshop_num = getshop_num;
module.exports.getid_shop = getid_shop;

