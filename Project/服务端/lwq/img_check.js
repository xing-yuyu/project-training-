const request = require('request')

const getAccessToken = () => {
  return new Promise((resolve, reject) => {
    const url = `https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxa9d0987bf6537b82&secret=b3ff596bd138b55e50ff547ba8412105`
      request(url, function(error, response, body) {
        if(error) {
          reject(error)
        } else {
          const res = JSON.parse(body)
          resolve(res.access_token)
       }
     })
  })
}

const imgSecCheck = (access_token, media) => {
  return new Promise((resolve, reject) => {
    const url = `https://api.weixin.qq.com/wxa/img_sec_check?access_token=${access_token}`
      request.post({
        url,
        headers: {
          'content-type': 'application/json; charset=utf-8'
        },
        formData: { media },
        }, function(error, response, body) {
          if(error) {
            reject(error)
          } else {
            resolve(body)
          }
      })
    })
}

module.exports = {
  getAccessToken,
  imgSecCheck,
}
