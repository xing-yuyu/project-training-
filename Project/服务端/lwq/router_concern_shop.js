const mysql = require('mysql2/promise'),
      log   = console.log,
      Koa   = require('koa'),
      app   = new Koa(),
      Router = require('koa-router'),
      router = new Router(),
      fs    = require('fs'),
      path  = require('path'),
      cors  = require('koa2-cors'),
      http  = require('http'),
      https = require('https'),
      body = require('koa-bodyparser'),
      hq = require('./concern_shop.js'),
      hq1 = require('./shop.js');

const options = {
    key:fs.readFileSync(path.join(__dirname,'./fourgoldhua.cn.key')),
    cert:fs.readFileSync(path.join(__dirname,'./fourgoldhua.cn.crt'))
}
app.use(body());
app.use(cors({
    origin: function (ctx) {
        if (ctx.url === '/test') {
          return "*"; // 允许来自所有域名请求
        }
        return 'http://localhost:8080'; // 这样就能只允许 http://localhost:8080 这个域名的请求了 
    },
    exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
    maxAge: 5,
    credentials: true,
    allowMethods: ['GET', 'POST', 'DELETE'],
    allowHeaders: ['Content-Type', 'Authorization', 'Accept'],
}))
//顺序
function sortx(a,b){
  return b.date-a.date;
}
//反序
function sortj(a,b){
  return Date.parse(a.date)-Date.parse(b.date);
}
//id筛选
function food(data,id){
    for(var i = 0;i < data.length;i++){
       if(data[i].food!=id){
           data.splice(i,1);
              i-=1;
       }
    }
    return data;
}
//删空
function climg (obj, fn) {
  for (var i in obj) {
      if (typeof obj[i] === 'object') {
            climg(obj[i], fn)
      }
      if (fn(obj[i])) {
          delete obj[i]
      }
  }
}
 
function delimg (foo) {
    if (typeof foo === 'object') {
        for (var i in foo) {
             return false
        }
        return true
    } else {
        return foo === '' || foo === null || foo === undefined
    }
}
//调用 climg(imgs,delimg)

router.get('/concern/shop',async (ctx)=>{
  let {user_id,shop_id,state} = ctx.request.query;
  //console.log(user_id);
  //console.log(shop_id);
  //console.log(state);
  let state1;
  if(state == 2){
    await hq.insertuser_shop(user_id,shop_id);
    state1=1;
  }
  else if(state == 1) {
    await hq.updateuser_shop(user_id,shop_id,0);
    state1=0;
  }
  else if(state == 0){
    await hq.updateuser_shop(user_id,shop_id,1);
    state1=1;
  }
  let arr = await hq.getshop_num(shop_id,1);
  num = arr.length;
  let arr1=[];
  arr1.push({
    "num":num,
    "state":state1
  })
  log(arr1);
  ctx.body = JSON.stringify(arr1);
})
//查询用户已经关注的商家
router.get('/concern/user_shop',async (ctx)=>{
  let {user_id} = ctx.request.query;
  let arr = await hq.getuser_shop(user_id,1);
  let crr=[];
  for(let i =0 ;i<arr.length;i++){
    brr = await hq.getid_shop(arr[i].shop_id);
    crr.push(brr[0])
    //log(brr);
  }
  log(crr);
  //log(arr);
  ctx.body = JSON.stringify(crr);
})

process.on('unhandledRejection',(reason,p) => {
    console.log('Unhandled Rejection at:Promise',p,'reason:',reason);
});

app.use(router.routes())
   .use(router.allowedMethods());
//app.listen(2227);


https.createServer(options,app.callback()).listen(4244);

