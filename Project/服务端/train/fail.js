const Koa = require('koa'),
      app = new Koa(),
      Router = require('koa-router'),
      router = new Router,
      http = require('http');


var config = require('./wxkey.js');
console.log(config);
// querystring这个模块，用来做url查询参数的解析
const querystring = require('querystring');
const request = require('request');
// 引入之后,后面的appId是在config内的appId名字
const appId = config.appid;
const appSecret = config.secret;

router.post('/wxgetId',function (req,res){
  console.log(req.request);

  console.log(appId);
  var data = {
    'appid':appId,
    'secret':appSecret,
    'js_code':req.body.code,
    'grant_type':'authorization_code'
  };
  console.log(data);

  var content = querystring.stringify(data);
  var url = 'https://api.weixin.qq.com/sns/jscode2session?' + content;
  console.log(url);
  request.get({
    'url':url
  },(error, response, body) => {
    let abody = JSON.parse(body);
    console.log(abody);
    res.json(abody);
  })
})

app.use(router.routes())
   .use(router.allowedMethods());

app.listen(3003);
