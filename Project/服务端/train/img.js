#!/usr/bin/env node

const http = require('http'),
      url = require('url'),
      fs = require('fs'),
      log = console.log;

http.createServer((req,res)=>{
  if(req.method === 'GET'){
    
    try {
      var add = url.parse(req.url);
      var pathname = add.pathname;
      if(fs.statSync('.'+pathname).isFile()){
        //读取到是文件返回文件可读流
        if(pathname != '/favicon.ico'){
          res.writeHead(200,{
            'Content-Type':'image/jpg',
          });
          fs.createReadStream(`.${pathname}`).pipe(res);
        }
      }else{
        //读取到是文件夹返回404
        res.writeHead(200,{
          'Content-Type':'text/html;charset=utf-8'
        });
        res.end('<h1 style="text-align:center">404</h1><h1 style="text-align:center">找不到网页啦！</h1>');
      }
    } catch(e) {
      log(e);
    }
  }
}).listen(3001);


