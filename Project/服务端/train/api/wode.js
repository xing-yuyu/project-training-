const con = require('./con.js'),
      fs  = require('fs'),
      log = console.log,
      Koa = require('koa'),
      app = new Koa(),
      https = require('https'),
      sslify = require('koa-sslify').default,
      Router = require('koa-router'),
      router = new Router();
      
const options = {
  key: fs.readFileSync('../fourgoldhua.cn.key'),
  cert: fs.readFileSync('../fourgoldhua.cn.crt')
}

app.use(sslify());
const bodyParser = require('koa-bodyparser');
app.use(bodyParser());

async function getll(id1){
  const sql = `select share_id,title,img1,size from main_table left join img_table on share_id = img_table.id where user_id = '${id1}' order by share_id desc`;
  let [rows] = await con.execute(sql);
  return rows;
}

//(async function (){
  //console.log(await getll());
//})();

router.post('/getwd',async (ctx) => {
  
  let ac = ctx.request.body.openid;
  //console.log(ctx.request.body.openid);
  //console.log('ac:      ',ac);
  let arr = await getll(`${ac}`);
  //console.log('arr:     ',arr);
  ctx.body = arr;
})

app.use(router.routes())
   .use(router.allowedMethods());

//app.listen(3004);
https.createServer(options,app.callback()).listen(3004);
