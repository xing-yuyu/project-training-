const fs = require('fs');
const path = require('path');
const mime = require('mime-types');
const Koa = require('koa');
const app = new Koa();


app.use(async (ctx) => {
  let filePath = path.join(__dirname,ctx.url);
  let file = null;
  try {
    file = fs.readFileSync(filePath);
  } catch (error){
    filePath = path.join(__dirname,'/');
    file = fs.readFileSync(filePath);
  }

  let mimeType = mime.lookup(filePath);
  ctx.set('content-type',mimeType);
  ctx.body = file;

})

app.listen(3001);
