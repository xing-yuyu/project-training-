import {
  request
} from "../../request";
import {regeneratorRuntime} from "../../libs/runtime/runtime";

Page({
  /**
   * 页面的初始数据
   */
  data: {
    idx:'',//share_id
    shopdata_item:{},
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('detaile3页面:' + this.options.idx);
    this.setData({
      idx: this.options.idx
    })
    this.getShopData_item(this.options.idx);
  },
  //获取店家详情页面信息
  async getShopData_item(idx) {
    let user_id = wx.getStorageSync('openid')
    const res = await request({
      url: '/shop/api/details',
      data: {
        user_id,
        shop_id: idx
      }
    });
    console.log(res)
    this.setData({
      shopdata_item:res[0]
    })
    console.log(res[0])
    wx.setStorageSync("shop_postion", {
      time: Date.now(),
      data: this.data.shopdata_item.shop_jwd
    })
  },
  //关注店家
  guanzhushop(){
    let that = this
    let shop_id  = this.data.idx
    let message = this.data.shopdata_item
    let state = message.state
    let likeFlag = false; //标志，避免多次发请求
    //避免多次点击
    if (likeFlag === true) {
      return false;
    }
    if (message.state == 1) {
      message.num =parseInt(message.num) - 1
      message.state = 0
      console.log("取消关注")
    } else {
      message.num = parseInt(message.num) + 1
      message.state = 1
      console.log("关注成功")
    }
    that.setData({
      shopdata_item: message
    })
    this.guanzhu_shopdetails(shop_id,state)
  },
  
  async guanzhu_shopdetails(shop_id,state) {
    let user_id = wx.getStorageSync('openid')
    const res = await request({
      url: '/concern/shop',
      data: {
        user_id,
        shop_id,
        state
      }
    });
  },
  //跳转美图外卖
  handleClick() {
    wx.navigateToMiniProgram({
      appId: 'wx2c348cf579062e56',
      path: '/packages/restaurant/restaurant/restaurant?poi_id=' + this.data.shopdata_item.shop_mtid + '&aas=1003&cat_id=0'
    })
  },
  //打电话
  phonecall: function () {
    wx.makePhoneCall({
      phoneNumber: this.data.shopdata_item.shop_tel //仅为示例，并非真实的电话号码
    })
  },
  //跳转地图页面
  jumpto_map: function () {
    wx.navigateTo({
      url: '/pages/navigation_car/navigation',
    })
  },
  jumptofoodsecurity: function () {
    let index = this.data.idx
    wx.navigateTo({
      url: `/pages/foodsecurity/foodsecurity?idx=${index}`,
    })
  },
})