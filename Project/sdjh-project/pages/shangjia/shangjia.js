import { request } from "../../request";
import regeneratorRuntime from "../../libs/runtime/runtime";
Page({
  onLoad: function (options) {
    this.getTalks();
  },
  async getTalks() {
    //使用async await发送异步请求
    let user_id = wx.getStorageSync('openid')
    const res = await request({
      url: '/concern/user_shop',
      data: {
        user_id
      }
    });
    this.setData({
      Shoplist: res
    })
    console.log(res)
  },
  jumpto_shop_detail: function (e) {
    console.log(e.currentTarget.dataset.index);
    console.log(this.data.Shoplist)
    let index = this.data.Shoplist[e.currentTarget.dataset.index].id
    wx.navigateTo({
      url: `/pages/detail3/detail3?idx=${index}`,
    })

  },
})