import {
  request
} from "../../request";
import {
  regeneratorRuntime
} from "../../libs/runtime/runtime";
Page({
  data: {
    img: {},
    idx: '', //分享页面id
    talks: [], //页面展示的评论区
    inputValue: [], //输入框的值
  },
  //console.log(this.data.img.user_id)
  onLoad: function (options) {
    this.sX()
    console.log('detaile1页面:' + this.options.idx);
    //物尽其用 保存写过的数据
    let intput_value = wx.getStorageSync('inputValue')
    this.setData({
      idx: this.options.idx,
      inputValue: intput_value
    })
    this.getguanzhustate(this.options.idx)
    this.getshoucangstate(this.options.idx)
    this.getdetaildata(this.options.idx)
    this.getdetaildata2(this.options.idx)
    this.getdetaildata3(this.options.idx)
    this.setData({
      currentSwiper: 0,
    });
    this.getTalks(this.options.idx)
  },
  swiperChange: function (e) {
    this.setData({
      currentSwiper: e.detail.current
    })
  },
  //得到收藏状态
  async getshoucangstate(share_id) {
    const res = await request({
      url: '/getconcern',
      data: {
        openid: wx.getStorageSync('openid'),
        share_id
      }
    });
    this.setData({
      shoucangstate: res[0].state
    })
    console.log(res)
  },
  //改变收藏状态
  Changeshoucangstate: function (e) {
    let state = e.currentTarget.dataset.state
    let share_id = this.data.idx
    if (wx.getStorageSync('userInfo') === '') {
      wx.showModal({
        title: '您还未登录',
        content: '请点击确定跳转登录',
        success(res) {
          if (res.confirm) {
            wx.switchTab({
              url: '/pages/mine/mine',
            })
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    } else {
      this.UpdataShoucang(share_id, state);
    }
  },
  //改变收藏状态
  async UpdataShoucang(share_id, state) {
    let likeFlag = false; //标志，避免多次发请求
    //避免多次点击
    if (likeFlag === true) {
      return false;
    }
    const res = await request({
      url: '/concern/collect',
      data: {
        user_id: wx.getStorageSync('openid'),
        collect_id: share_id,
        state
      }
    });
    console.log(res)
    let that = this;
    that.setData({
      shoucangstate:res[0].state
    })
    console.log(res)
    if (res[0].state == 1) {
      wx.showToast({
        title: '已经收藏',
        duration: 1000,
        mask: false
      })
    } else {
      wx.showToast({
        title: '已经取消收藏',
        duration: 1000,
        mask: false
      })
    }
  },
  //得到guanzhustate
  async getguanzhustate(idx) {
    const user_id = wx.getStorageSync('openid')
    const res = await request({
      url: '/getconcern',
      data: {
        user_id,
        share_id: idx
      }
    });
    this.setData({
      guanzhustate: res[0].state
    })
    console.log(res)
  },
  //改变guanzhustate
  Changeguanzhustate: function (e) {
    let state = e.currentTarget.dataset.state
    let user_id = wx.getStorageSync('openid')
    let dav_id = this.data.Publisher.id
    if (wx.getStorageSync('userInfo') === '') {
      wx.showModal({
        title: '您还未登录',
        content: '请点击确定跳转登录',
        success(res) {
          if (res.confirm) {
            wx.switchTab({
              url: '/pages/mine/mine',
            })
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    } else {
      if (user_id == dav_id) {
        wx.showToast({
          title: '不能关注自己',
          image: '../../icon/error.png'
        })
        return
      }
      this.cgzs(dav_id, state);
    }
  },
  //改变关注状态
  async cgzs(dav_id, state) {
    //使用async await发送异步请求
    let user_id = wx.getStorageSync('openid')
    const res = await request({
      url: '/concern/dav',
      data: {
        user_id,
        dav_id,
        state
      }
    });
    if (res[0].state == 1)
      wx.showToast({
        title: '关注成功',
        duration: 1000,
        mask: false
      })
    else
      wx.showToast({
        title: '取关成功',
        duration: 1000,
        
      })
    this.setData({
      guanzhustate: res[0].state
    })
  },
  // 请求详情页数据
  async getdetaildata(idx) {
    const res = await request({
      url: '/getImg',
      data: {
        share_id: idx
      },
      method: 'POST',
    });
    let img = [];
    console.log(res)
    for (let index in res[0]) {
      //console.log(arr[0][index]);
      if (res[0][index] === null) {
        break
      }
      img.push(res[0][index]);
      this.setData({
        ImgUrl: img.slice(1, img.length)
      })
    }
  },
  async getdetaildata2(idx) {
    const res = await request({
      url: '/getUser',
      data: {
        share_id: idx
      },
      method: 'POST',
    });
    this.setData({
      Publisher: res[0]
    })
    console.log(res[0])
  },
  async getdetaildata3(idx) {
    const res = await request({
      url: '/getCt',
      method: 'POST',
      data: {
        share_id: idx
      },
    });
    this.setData({
      PublisherInfo: res[0]
    })
  },
  //动态获取评论区
  async getTalks(idx) {
    let user_id = wx.getStorageSync('openid')
    const res = await request({
      url: '/comments/shuju',
      method: "POST",
      data: {
        share_id: idx,
        user_id
      }
    });
    this.setData({
      talks: res
    })
    console.log(this.data.talks)
  },
  //请求发送评论
  async sendTalks() {
    let that = this;
    const nickName = wx.getStorageSync('userInfo').nickName
    const avatarUrl = wx.getStorageSync('userInfo').avatarUrl
    const openid = wx.getStorageSync('openid')
    const res = await request({
      url: '/comments',
      method: "POST",
      data: {
        share_id: this.data.idx,
        user_id: openid,
        date: (new Date()).getTime(),
        text: that.data.inputValue
      },
    });
    console.log('发送的内容' + that.data.inputValue)
    if (res.arr == 1) {
      wx.showToast({
        title: '发送的内容不合法',
        duration: 1000
      })
      return
    }
    console.log("发送评论成功")
    this.data.talks.unshift({
        share_id: this.data.idx,
        user_name: nickName, //local
        user_img: avatarUrl, //local
        date: '刚刚',
        text: that.data.inputValue,
        zan: 0,
        state: false
      }),
      that.setData({
        talks: that.data.talks,
        inputValue: ''
      })
    wx.removeStorageSync('inputValue')
  },
  //请求点赞
  async sendZan(comments_id, state) {
    let user_id = wx.getStorageSync('openid')
    const res = await request({
      url: '/zan',
      data: {
        user_id,
        comments_id,
        state
      },
    });
    console.log(res)
    if (res[0].state == 1) {
      wx.showToast({
        title: '点赞成功',
      })
    } else {
      wx.showToast({
        title: '取消成功',
      })
    }
  },
  //点赞
  favorclick: function (e) {
    let likeFlag = false; //标志，避免多次发请求
    //避免多次点击
    if (likeFlag === true) {
      return false;
    }
    let comments_id = e.currentTarget.dataset.id
    let state = e.currentTarget.dataset.state
    if (!comments_id) {
      wx.showToast({
        image: '../../icon/error.png',
        title: '不能点赞刚发的评论',
      })
      return
    }
    console.log(comments_id)
    let index = e.currentTarget.dataset.index
    let that = this
    let message = this.data.talks
    for (let i in message) {
      if (i == index) {
        if (message[i].state == 0) {
          message[i].zan = parseInt(message[i].zan) + 1
          that.data.talks[i].state = 1
        } else {
          message[i].zan = parseInt(message[i].zan) - 1
          that.data.talks[i].state = 0
        }
      }
    }
    that.setData({
      talks: message
    })
    this.sendZan(comments_id, state)
  },
  //刷新更新本地输入框输入值
  refresh_inputvalue(e) {
    console.log(e.detail.value)
    if (e.detail.value.length >= 50) {
      wx.showToast({
        title: '评论最多50字',
        icon: false,
        duration: 2000
      })
      return
    } else {
      this.setData({
        inputValue: e.detail.value
      })
      wx.setStorageSync('inputValue', e.detail.value)
    }
  },
  //输入框失去焦点时触发
  bindInputBlur: function (e) {
    this.data.inputValue = e.detail.value
    wx.setStorageSync('inputValue', e.detail.value)
  },
  //点击按键发布评论
  faBupinglun: function () {
    //判断输入评论区输入是否为空
    if (!this.data.inputValue) {
      wx.showToast({
        title: '不能输入为空',
        image: '../../icon/error.png',
        duration: 2000,
        mask: false
      })
      return
    }
    //判断用户是否登录
    if (!wx.getStorageSync("userInfo")) {
      wx.showToast({
        title: '您还没有登录',
        image: '../../icon/error.png',
        duration: 2000,
        mask: false
      })
      return
    }
    this.Post_refresh()
  },
  //回车发布评论
  Sendpinglun: function (e) {
    this.data.inputValue = e.detail.value
    if (this.data.inputValue)
      this.Post_refresh()
    else
      wx.showToast({
        title: '不能输入为空偶',
        image: '../../icon/error.png',
        duration: 2000,
        mask: false
      })
  },
  //上传并更新评论
  Post_refresh: function () {
    //上传数据
    this.sendTalks()
  },
  jumptodetail1address: function () {
    let index = this.data.img.jwd
    console.log(index)
    wx.navigateTo({
      url: `/pages/detail1address/detail1address?idx=${index}`,
    })
  },
  async sX() {
    const res = await request({
      url: '/shuju/sx',
    });
    if (res.data === 1) {
      this.setData({
        sx: true
      })
    }
  },
  onScrollLoad() {
    wx.showToast({
      title: '评论已经到底了',
      duration: 2000,
      mask: false
    })
  }
})