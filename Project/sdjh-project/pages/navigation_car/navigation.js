var amapFile = require('../../libs/amap-wx.js');
var config = require('../../libs/config.js');

Page({
  data: {
    markers: [{
      iconPath: "../../img/mapicon_navi_s.png",
      id: 0,
      latitude: 30.997898,
      longitude: 114.520805,
      width: 23,
      height: 33
    },{
      iconPath: "../../img/mapicon_navi_e.png",
      id: 1,   
      latitude: 37.977039,
      longitude: 114.528778,
      width: 24,
      height: 34
    }],
    distance: '',
    cost: '',
    polyline: []
  },
 
  onLoad: function() {
    let user_postion = wx.getStorageSync("user_postion");
    let shop_postion = wx.getStorageSync("shop_postion");
    if (!user_postion) {
      wx.showModal({
        title: '用户位置',
        content: '需要位置',
        success (res) {
          if (res.confirm) {
            console.log('用户点击确定')
            wx.openSetting({
              success (res) {
                console.log(res.authSetting)
                this.getJw();
              }
            })
          } else if (res.cancel) {
            console.log('又拒绝了 啊这还没解决呢')
            
          }
        }
      })
    } 
    else if(Date.now() - user_postion.time > 1000 * 600){
      this.getJw()
    }
    else{
      console.log("使用本地的用户位置信息")
    }
    let latitude_s = user_postion.data.split(',')[0]
    let longitude_s = user_postion.data.split(',')[1]
    console.log("纬度"+user_postion.data.split(',')[0]);
    console.log("经度"+user_postion.data.split(',')[1]);

    let latitude_e = shop_postion.data.split(',')[0]
    let longitude_e = shop_postion.data.split(',')[1]
    console.log("纬度"+shop_postion.data.split(',')[0]);
    console.log("经度"+shop_postion.data.split(',')[1]);
    let that = this;
    that.setData({
      markers: [{
        iconPath: "../../img/mapicon_navi_s.png",
        id: 0,
        latitude: latitude_s,
        longitude: longitude_s,
        width: 23,
        height: 33
      },{
        iconPath: "../../img/mapicon_navi_e.png",
        id: 1,   
        latitude: latitude_e,
        longitude: longitude_e,
        width: 24,
        height: 34
      }]
    })


    var key = config.Config.key;
    var myAmapFun = new amapFile.AMapWX({key: key});
    myAmapFun.getDrivingRoute({
      //起始点经纬
      origin:that.data.markers[0].longitude+','+that.data.markers[0].latitude,
      destination: that.data.markers[1].longitude+','+that.data.markers[1].latitude,
      success: function(data){
        var points = [];
        if(data.paths && data.paths[0] && data.paths[0].steps){
          var steps = data.paths[0].steps;
          for(var i = 0; i < steps.length; i++){
            var poLen = steps[i].polyline.split(';');
            for(var j = 0;j < poLen.length; j++){
              points.push({
                longitude: parseFloat(poLen[j].split(',')[0]),
                latitude: parseFloat(poLen[j].split(',')[1])
              })
            } 
          }
        }
        that.setData({
          polyline: [{
            points: points,
            color: "#0091ff",
            width: 6
          }]
        });
        if(data.paths[0] && data.paths[0].distance){
          that.setData({
            distance: data.paths[0].distance + '米'
          });
        }
        if(data.taxi_cost){
          that.setData({
            cost: '打车约' + parseInt(data.taxi_cost) + '元'
          });
        }
          
      }
    })
  },
  getJw: function () {
    wx.getLocation({
      type: 'gcj02',
      success: function (res) {
        var latitude = res.latitude
        var longitude = res.longitude
        var user_postion = latitude + ',' + longitude;
        console.log("使用接口拿到的实时用户位置信息"+user_postion)
        wx.setStorageSync("user_postion", {
          time: Date.now(),
          data: user_postion
        });

      }
    })
  },

  goDetail: function(){
    wx.navigateTo({
      url: '../navigation_car_detail/navigation'
    })
  },
  goToCar: function (e) {
    wx.redirectTo({
      url: '../navigation_car/navigation'
    })
  },
  goToBus: function (e) {
    wx.redirectTo({
      url: '../navigation_bus/navigation'
    })
  },
  goToRide: function (e) {
    wx.redirectTo({
      url: '../navigation_ride/navigation'
    })
  },
  goToWalk: function (e) {
    wx.redirectTo({
      url: '../navigation_walk/navigation'
    })
  }
})