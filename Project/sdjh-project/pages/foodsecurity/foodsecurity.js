// pages/foodsecurity/foodsecurity.js
import {
  request
} from "../../request";
import {regeneratorRuntime} from "../../libs/runtime/runtime";
Page({
  data: {

  },
  onLoad: function (options) {
    console.log('视频安全档案' + this.options.idx);
    //this.getimages(this.options.idx);
    let arr = [{url:'http://fourgoldhua.cn:3001/shop/zql/file1.jpg'},{url:'http://fourgoldhua.cn:3001/shop/zql/file2.jpg'}]
    this.setData({
      images:arr
    })
  },
  async getimages(idx) {
    let user_id = wx.getStorageSync('openid')
    const res = await request({
      url: ':2228/shop/api/details',
      data: {
        user_id:user_id,
        shop_id: idx
      }
    });
    this.setData({
      images:res
    })
    console.log(res)
  },
  handlePrevewimg:function(e){
    const urls =this.data.images.map(v=>v.url)
    const current = e.currentTarget.dataset.url;
    wx.previewImage({
      current:current,
      urls: urls,
    })
  }
})