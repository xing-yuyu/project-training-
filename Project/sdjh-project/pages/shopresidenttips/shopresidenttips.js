import {
  request
} from "../../request";
// pages/shopresidenttips/shopresidenttips.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //页面展示数据
    tips:[
      {
        imgsrc:'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.wdjl.net%2Fuploads%2Fallimg%2F200415%2F15-200415125923.jpg&refer=http%3A%2F%2Fwww.wdjl.net&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1623746841&t=d51d13af0c309eef7d1cf1692cf7ab69',
        title:'实体店铺图',
        content:'需提供有完整牌匾的门脸图和真实用餐环境'
      },
      {
        imgsrc:'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fp0.meituan.net%2Fdeal%2F__30185921__2659464.jpg&refer=http%3A%2F%2Fp0.meituan.net&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1623746941&t=8d96c7d152a1ab25d97609bb169f44ba',
        title:'实体店铺图',
        content:'需提供有完整牌匾的门脸图和真实用餐环境'
      },
      {
        imgsrc:'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fp0.meituan.net%2Fdeal%2F__30185921__2659464.jpg&refer=http%3A%2F%2Fp0.meituan.net&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1623746941&t=8d96c7d152a1ab25d97609bb169f44ba',
        title:'实体店铺图',
        content:'需提供有完整牌匾的门脸图和真实用餐环境'
      },
      {
        imgsrc:'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fp0.meituan.net%2Fdeal%2F__30185921__2659464.jpg&refer=http%3A%2F%2Fp0.meituan.net&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1623746941&t=8d96c7d152a1ab25d97609bb169f44ba',
        title:'实体店铺图',
        content:'需提供有完整牌匾的门脸图和真实用餐环境'
      },
    ],
    //协议是否同意
    agree:false,
    ondisplay: 'none'
  },
  onLoad: function () {
    this.sX();
  },
  //boxcheck改变事件
  boxcheck:function(e){
    //console.log(e.detail.value)
    let {agree} = this.data
    this.setData({
      agree:!agree
    })
  },
  //跳转协议页面
  jumpto_residentlogin: function (e) {
    if(!this.data.agree){
      wx.showToast({
        title: '请同意下方协议',
      })
      return
    }
    wx.navigateTo({
      url: '/pages/shopresidentlogin/shopresidentlogin',
    })
  },
  async sX() {
    const res = await request({
      url: '/shuju/sx',
    });
    if(res.data === 1){
      this.setData({
        ondisplay: 'block'
      })
    }
  },
})