import { request } from "../../request";
import {regeneratorRuntime} from "../../libs/runtime/runtime";
Page({
  data: {
    fens:[]
  },
  onLoad: function (options) {
    this.getTalks();
  },
  //获取关注信息
  async getTalks() {
    const res = await request({
      url: '/wd/dav',
      data: {
        user_id:wx.getStorageSync('openid')
      }
    });
    this.setData({
      fens: res
    })
    console.log(res)
  },
  //改变关注状态
  changzs:function(e){
    let index = e.currentTarget.dataset.index
    let dav_id = e.currentTarget.dataset.dav
    let state = e.currentTarget.dataset.state
    this.cgeGzs(dav_id,state,index);
  },
  async cgeGzs(dav_id,state,index) {
    const res = await request({
      url: '/wd/quguan', 
      data: {
        user_id:wx.getStorageSync('openid'),
        dav_id,
        state
      }
    })
    console.log(res)
    let that = this;
    let fen = this.data.fens;
    fen[index].state=res[0].state
    that.setData({
      fens:fen
    })
  },
  //搜索回调
  searchList(ev) {
    let likeFlag = false; //标志，避免多次发请求
    //避免多次点击
    if (likeFlag === true) {
      return false;
    }
    if(!ev.detail.detail.value.trim())
      return
    this.setData({
      searchstr: ev.detail.detail.value
    })
    this.getSearchData(this.data.searchstr)
  },
  //搜索回调
  endsearchList() {
    console.log(this.data.searchstr, '搜索回调函数')
    this.getSearchData(this.data.searchstr)
  },
  //获取搜索数据
  async getSearchData(e) {
    if(e){
      const res = await request({
        url: '/wd/ss',
        data: {
          user_id:wx.getStorageSync('openid'),
          name: e
        }
      });
      if(res){
        this.setData({ 
          fens:res
        })
      }
      this.setData({ 
        fens:[]
      })
      console.log(res)
    }
  },
  // 取消搜索
  cancelsearch() {
    this.setData({
      searchstr: ''
    })
    this.getTalks();
  },
  //清空搜索框
  activity_clear(e) {
    this.setData({
      searchstr: ''
    })
    this.getTalks();
  },
})