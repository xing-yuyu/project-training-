// pages/details2/details2.js
import {
  request
} from "../../request";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgarray: [],
    imgarray1: [],
    imgarray2: [],
    imgarray3: [],
    shop_name: '商家信息错误',
    shop_tel: '',
    score: [2, 2, 2, 2, 2],
    score1: '0.0',
    shop_addr: "暂无地址",
    shop_time: "8:00-20:00",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.idx)
    let a = '4.5';
    this.setData({
      score1: a
    })
    // 评定星级，以0,1,2为标识，做为放置星星的标准
    // 初始值必须都为2，这样即使后面置为空值，也能保证星星全部显示
    let temp = this.data.score;
    let judge = false;
    for (let i = 0; i < 5; i++) {
      if (!judge) {
        if (i < parseInt(a)) {
          temp[i] = 0;
        } else {
          if (parseInt(a.split('.')[1]) >= 8) {
            temp[i] = 0;
          } else if (parseInt(a.split('.')[1]) < 8 && parseInt(a.split('.')[1]) >= 3) {
            temp[i] = 1;
          }
          judge = true;
        }
      }

    }
    this.setData({
      score: temp
    })
    this.getShopData_item(options.idx);
    console.log(this.data.imgarray);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  calling: function () {
    wx.makePhoneCall({
      phoneNumber: this.data.shop_tel, //此号码并非真实电话号码，仅用于测试
      success: function () {
        console.log("拨打电话成功！")
      },
      fail: function () {
        console.log("拨打电话失败！")
      }
    })
  },
  async getShopData_item(idx) {
    // let open_id = wx.getStorageSync('openid')
    const res = await request({
      url: '/shoppage/yonghu',
      data: {
        id: idx,
        // shop_id: idx
      }
    });
    let temp0 = [],temp1 = [],temp2 = [],temp3 = [];
    for(let i in res.image0){
      temp0[i] = 'https://fourgoldhua.cn' + res.image0[i].img;
    }
    if(!res.image0){
      console.log('bucunzai')
    }
    for(let i in res.image1){
      temp1[i] = 'https://fourgoldhua.cn' + res.image1[i].img;
    }
    for(let i in res.image2){
      temp2[i] = 'https://fourgoldhua.cn' + res.image2[i].img;
    }
    for(let i in res.image3){
      temp3[i] = 'https://fourgoldhua.cn' + res.image3[i].img;
    }
    this.setData({
      imgarray: temp0,
      imgarray1: temp1,
      imgarray2: temp2,
      imgarray3: temp3,
      shop_name: res.data[0].shop_name,
      shop_addr: res.data[0].shop_addr,
      shop_tel: res.data[0].shop_tel,
      shop_time: res.data[0].shop_time
    })
    console.log(res);
  },
  previewImg:function(e){
    var index = e.currentTarget.dataset.index;
    var imgArr = this.data.imgarray;
    wx.previewImage({
      current: imgArr[index],     //当前图片地址
      urls: imgArr,               //所有要预览的图片的地址集合 数组形式
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  previewImg1:function(e){
    var index = e.currentTarget.dataset.index;
    var imgArr = this.data.imgarray1;
    wx.previewImage({
      current: imgArr[index],     //当前图片地址
      urls: imgArr,               //所有要预览的图片的地址集合 数组形式
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  previewImg2:function(e){
    var index = e.currentTarget.dataset.index;
    var imgArr = this.data.imgarray2;
    wx.previewImage({
      current: imgArr[index],     //当前图片地址
      urls: imgArr,               //所有要预览的图片的地址集合 数组形式
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  previewImg3:function(e){
    var index = e.currentTarget.dataset.index;
    var imgArr = this.data.imgarray3;
    wx.previewImage({
      current: imgArr[index],     //当前图片地址
      urls: imgArr,               //所有要预览的图片的地址集合 数组形式
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },

})
