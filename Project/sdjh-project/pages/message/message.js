import { request } from "../../request";
import {regeneratorRuntime} from "../../libs/runtime/runtime";
Page({
  data: {
    message:{}
  },
  onLoad: function () {
    this.getTalks();
  },
  onShow:function(){
    this.getTalks();
  },
  jumpdetail1: function (e) {
    let id = e.currentTarget.dataset.index;
    wx.navigateTo({
      url: `/pages/detail1/detail1?idx=${id}`,
    })
  },
  //获取信息
  async getTalks() {
    const res = await request({
      url: '/wd/xx',
      data: {
        user_id:wx.getStorageSync('openid')
      }
    });
    this.setData({
      message: res
    })
    console.log(res)
  },
  del_pingjia:function(e){
    this.delPj(e.currentTarget.dataset.index);
  },
  async delPj(share_id) {
    const res = await request({
      url: '/wd/xx/del',
      data: {
        share_id,
        user_id:wx.getStorageSync('openid'), 
      }
    })
    console.log(res)
    this.setData({
      message: res
    })
  },
})