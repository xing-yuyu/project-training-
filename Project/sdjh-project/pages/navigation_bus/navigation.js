var amapFile = require('../../libs/amap-wx.js');
var config = require('../../libs/config.js');
//37.997898,114.520805 河北师大
//37.977039,114.528778
Page({
  data: {
    markers: [{
      iconPath: "../../img/mapicon_navi_s.png",
      id: 0,
      latitude: 37.997898,
      longitude: 114.520805,
      width: 23,
      height: 33
    },{
      iconPath: "../../img/mapicon_navi_e.png",
      id: 1,
      latitude: 37.977039,
      longitude: 114.528778,
      width: 24,
      height: 34
    }],
    distance: '',
    cost: '',
    transits: [],
    polyline: []
  },
  onLoad: function() {
    let user_postion = wx.getStorageSync("user_postion");
    let shop_postion = wx.getStorageSync("shop_postion");
    if (!user_postion) {
      wx.showModal({
        title: '用户位置',
        content: '需要位置',
        success (res) {
          if (res.confirm) {
            console.log('用户点击确定')
            wx.openSetting({
              success (res) {
                console.log(res.authSetting)
                this.getJw();
              }
            })
          } else if (res.cancel) {
            console.log('又拒绝了 啊这还没解决呢')
            
          }
        }
      })
    } 
    else if(Date.now() - user_postion.time > 1000 * 600){
      this.getJw()
    }
    else{
      console.log("使用本地的用户位置信息")
    }
    let latitude_s = user_postion.data.split(',')[0]
    let longitude_s = user_postion.data.split(',')[1]
    let latitude_e = shop_postion.data.split(',')[0]
    let longitude_e = shop_postion.data.split(',')[1]
    let that = this;
    that.setData({
      markers: [{
        iconPath: "../../img/mapicon_navi_s.png",
        id: 0,
        latitude: latitude_s,
        longitude: longitude_s,
        width: 23,
        height: 33
      },{
        iconPath: "../../img/mapicon_navi_e.png",
        id: 1,   
        latitude: latitude_e,
        longitude: longitude_e,
        width: 24,
        height: 34
      }]
    })
    
    var key = config.Config.key;
    var myAmapFun = new amapFile.AMapWX({key: key});
    myAmapFun.getTransitRoute({
      origin: that.data.markers[0].longitude+','+that.data.markers[0].latitude,
      destination: that.data.markers[1].longitude+','+that.data.markers[1].latitude,
      city: '石家庄',
      success: function(data){
        if(data && data.transits){
          var transits = data.transits;
          for(var i = 0; i < transits.length; i++){
            var segments = transits[i].segments;
            transits[i].transport = [];
            for(var j = 0; j < segments.length; j++){
              if(segments[j].bus && segments[j].bus.buslines && segments[j].bus.buslines[0] && segments[j].bus.buslines[0].name){
                var name = segments[j].bus.buslines[0].name
                if(j!==0){
                  name = '--' + name;
                }
                transits[i].transport.push(name);
              }
            }
          }
        }
        that.setData({
          transits: transits
        });
          
      },
      fail: function(info){

      }
    })
  },
  getJw: function () {
    wx.getLocation({
      type: 'gcj02',
      success: function (res) {
        var latitude = res.latitude
        var longitude = res.longitude
        var user_postion = latitude + ',' + longitude;
        console.log("使用接口拿到的实时用户位置信息"+user_postion)
    
        wx.setStorageSync("user_postion", {
          time: Date.now(),
          data: user_postion
        });

      }
    })
  },
  goToCar: function (e) {
    wx.redirectTo({
      url: '../navigation_car/navigation'
    })
  },
  goToBus: function (e) {
    wx.redirectTo({
      url: '../navigation_bus/navigation'
    })
  },
  goToRide: function (e) {
    wx.redirectTo({
      url: '../navigation_ride/navigation'
    })
  },
  goToWalk: function (e) {
    wx.redirectTo({
      url: '../navigation_walk/navigation'
    })
  }
})