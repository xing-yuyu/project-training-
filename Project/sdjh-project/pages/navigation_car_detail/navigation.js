var amapFile = require('../../libs/amap-wx.js');
var config = require('../../libs/config.js');

Page({
  data: {
    steps: {}
  },
  onLoad: function () {
    let user_postion = wx.getStorageSync("user_postion");
    let shop_postion = wx.getStorageSync("shop_postion");
    let latitude_s = user_postion.data.split(',')[0]
    let longitude_s = user_postion.data.split(',')[1]
    console.log("纬度" + user_postion.data.split(',')[0]);
    console.log("经度" + user_postion.data.split(',')[1]);

    let latitude_e = shop_postion.data.split(',')[0]
    let longitude_e = shop_postion.data.split(',')[1]
    console.log("纬度" + shop_postion.data.split(',')[0]);
    console.log("经度" + shop_postion.data.split(',')[1]);
    let that = this;
    that.setData({
      markers: [{
        iconPath: "../../img/mapicon_navi_s.png",
        id: 0,
        latitude: latitude_s,
        longitude: longitude_s,
        width: 23,
        height: 33
      }, {
        iconPath: "../../img/mapicon_navi_e.png",
        id: 1,
        latitude: latitude_e,
        longitude: longitude_e,
        width: 24,
        height: 34
      }]
    })

    var key = config.Config.key;
    var myAmapFun = new amapFile.AMapWX({
      key: key
    });
    myAmapFun.getDrivingRoute({
      origin: that.data.markers[0].longitude + ',' + that.data.markers[0].latitude,
      destination: that.data.markers[1].longitude + ',' + that.data.markers[1].latitude,
      success: function (data) {
        if (data.paths && data.paths[0] && data.paths[0].steps) {
          that.setData({
            steps: data.paths[0].steps
          });
        }

      },
      fail: function (info) {

      }
    })
  }
})