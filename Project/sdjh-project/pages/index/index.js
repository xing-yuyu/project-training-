import {
  request
} from "../../request";
import {regeneratorRuntime} from "../../libs/runtime/runtime";
Page({
  data: {
    Shoplist:[],//店家列表
  },
  QueryParams:{
    pageNum:0,
    pageSize:5,
  },
  totalPages:1,

  onLoad: function () {
    this.getShopData();
  },
  async getShopData() {
    const res = await request({
      url: '/shop_sc/api',
      data:this.QueryParams
    });
    console.log(res)
    const total = res.nums
    this.totalPages = Math.ceil(total/this.QueryParams.pageSize);
    this.setData({
      Shoplist:[...this.data.Shoplist,...res.data]
    })
    wx.stopPullDownRefresh()
  },
  /**
   * 
   * @param {滚动条触底事件} 
   */
  onReachBottom(){
    if(this.QueryParams.pageNum+1>=this.totalPages){
      wx.showToast({
        title: '已经下拉到底了',
        duration: 1000
      })
    }else{
      this.QueryParams.pageNum++;
      this.getShopData();
    }
  },
  /**
   * 
   * @param {下拉刷新事件} 
   */
  onPullDownRefresh(){
    this.setData({
      Shoplist:[]
    })
    this.QueryParams.pageNum=0;
    this.getShopData();
  },
  /**
   * 
   * @param {跳转页面} 商品详情页面和搜索页面
   */
  jumpto_shop_detail: function (e) {
    console.log(e.currentTarget.dataset.index);
    
    let index = this.data.Shoplist[e.currentTarget.dataset.index].id
    wx.navigateTo({
      url: `/pages/details2/details2?idx=${index}`,
    })

  },
  jumpto_search_page: function (e) {
    wx.navigateTo({
      url: '/pages/search/search',
    })
  },
  /*
    错误图片处理
  */
 errorFunction: function (e)  {
    let index = e.currentTarget.dataset.index
    let that =this
    let imglist = this.data.Shoplist
    console.log("图片错误位置"+index)
    //错误图片处理
    imglist[index].shop_img = '/shop/zql/shibai.jpg'
    that.setData({
      Shoplist: imglist
    })
  },
})