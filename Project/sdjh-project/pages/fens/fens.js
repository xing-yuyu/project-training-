import { request } from "../../request";
import {regeneratorRuntime} from "../../libs/runtime/runtime";
Page({
  onLoad: function (options) {
    this.getTalks();
  },
  async getTalks() {
    //使用async await发送异步请求
    let user_id = wx.getStorageSync('openid')
    console.log(user_id)
    const res = await request({
      url: '/wd/fensi',
      data: {
        user_id:user_id
      }
    });
    this.setData({
      fens: res
    })
    console.log(res)
  },
  //改变关注状态
  changzs:function(e){
    let index = e.currentTarget.dataset.index
    let fens_id = e.currentTarget.dataset.fens
    console.log(e)
    let state = e.currentTarget.dataset.state
    this.cgeGzs(fens_id,state,index);
  },
  async cgeGzs(fens_id,state,index) {
    //使用async await发送异步请求
    let user_id = wx.getStorageSync('openid')
    console.log(fens_id)
    console.log(index)
    const res = await request({
      url: '/wd/huguan',
      data: {
        user_id:user_id,
        fensi_id:fens_id,
        state:state
      }
    })
    console.log(res[0])
    let that = this;
    let fen = this.data.fens;
    fen[index].state=res[0].state
    that.setData({
      fens:fen
    })
  },
})