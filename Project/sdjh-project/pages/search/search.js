// pages/search/search.js
import {
  request
} from "../../request";
import regeneratorRuntime from "../../libs/runtime/runtime";
Page({
  data: {
    //搜索数据列表
    serchdata_list:[],
    //历史记录列表
    HistoryData: [],
    display:0
  },
  onLoad: function (options) {
    let pages = getCurrentPages();
    let prevpage = pages[pages.length - 2];
    console.log(prevpage.route)
    if(prevpage.route == 'pages/add1/add1'){
      this.setData({
        display:1
      })
    }
    let HistoryData=wx.getStorageSync("HistoryData")
    if(HistoryData){
      this.setData({
        HistoryData
      })
    }
  },
  onShow:function() {
    let HistoryData=wx.getStorageSync("HistoryData")
    if(HistoryData){
      this.setData({
        HistoryData
      })
    }
  },
  //跳转上传页面
  jumpto_add(e){
    let index = e.currentTarget.dataset.index
    console.log(this.data.serchdata_list[index])
    wx.navigateTo({
      url: `/pages/add1/add1?chooseAddress=${this.data.serchdata_list[index].shop_addr}&choosejwd=${this.data.serchdata_list[index].shop_jwd}&shop_id=${this.data.serchdata_list[index].id}&shop_name=${this.data.serchdata_list[index].shop_name}`,
    })
  },
  //跳转店铺详情页面
  jumpto_shop_detail: function (e) {
    let that = this;
    let index = that.data.serchdata_list[e.currentTarget.dataset.index].id
    let jump_shopname = that.data.serchdata_list[e.currentTarget.dataset.index].shop_name
    //将点击跳转过的数据存储到本地
    let hadserch = wx.getStorageSync('HistoryData')
    let flag = 1;
    for(let i=0;i<hadserch.length;i++){
      if(index == hadserch[i].id){
        flag=0;
      }
    }
    if(flag){
      let HistoryData=this.data.HistoryData
      HistoryData.unshift({
        id:index,
        history_str:jump_shopname
      }),
      wx.setStorageSync('HistoryData',HistoryData)
    }
    wx.navigateTo({
      url: `/pages/details2/details2?idx=${index}`,
    })
  },
  //搜索框输入时触发
  searchList(ev) {
    let likeFlag = false; //标志，避免多次发请求
    //避免多次点击
    if (likeFlag === true) {
      return false;
    }
    if(!ev.detail.detail.value.trim())
      return
    console.log(ev.detail.detail.value, '模糊查询字段')
    this.setData({
      searchstr: ev.detail.detail.value
    })
    this.getSearchData(this.data.searchstr)
  },
  //搜索回调
  endsearchList() {
    console.log(this.data.searchstr, '搜索回调函数')
    this.getSearchData(this.data.searchstr)
  },
  //获取搜索数据
  async getSearchData(e) {
    if(e){
      const res = await request({
        url: '/shoppage/name',
        data: {
          name: e
        }
      });
      console.log(res)
      let serchdata_list = res
      this.setData({
        serchdata_list
      })
    }
  },
  // 取消搜索
  cancelsearch() {
    this.setData({
      searchstr: '',
      serchdata_list:[]
    })
  },
  //清空搜索框
  activity_clear(e) {
    this.setData({
      searchstr: '',
      serchdata_list:[]
    })
  },
  //删除历史搜索记录
  his_del: function() {
    wx.removeStorageSync('HistoryData')
    this.setData({ 
      HistoryData:[]
    })
  },
  //历史记录跳转
  history_jump:function(e){
    let HistoryData = wx.getStorageSync("HistoryData")
    let HistoryShopid = HistoryData[e.currentTarget.dataset.index].id
    wx.navigateTo({
      url: `/pages/details2/details2?idx=${HistoryShopid}`,
    })
  }
})