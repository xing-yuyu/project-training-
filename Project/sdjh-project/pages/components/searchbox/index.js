// 本组件为搜索组件
Component({
  properties: {
    searchstr: {     //input  值
      type: String,
      value: ''
    },
    searchflag: {
      type: Boolean,
      value: false,
    },
    placeholder:{
      type: String,
      value: '搜索今日商品'
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    result: []
  },

  /**
   * 组件的方法列表

   */
  methods: {

    //获得焦点
    getfocus() {
      this.setData({
        searchflag: true,
      })
    },
    //搜索输入
    searchList(e) {
      this.triggerEvent("searchList", e);
    },
    //查询
    endsearchList(e) {
      this.triggerEvent("endsearchList");
    },
    //失去焦点
    blursearch() {
      console.log('失去焦点')
    },
    // 取消
    cancelsearch() {
      this.setData({
        searchflag: false,
      })
      this.triggerEvent("cancelsearch");
    },
    //清空搜索框
    activity_clear(e) {
      this.triggerEvent("activity_clear");
    },
  }
})
