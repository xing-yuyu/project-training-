Component({
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  /*
    自定义授权组件
  */ 
  properties: {
    // 授权标题
    title: {          
      type: String,     
      value: '标题' // 默认值
    },
    //授权内容
    content :{
      type : String ,
      value : '弹窗内容' // 默认值
    },

    //确认按钮文字
    confirmText :{
      type : String ,
      value : '确定' // 默认值
    },
    //授权主体图片
    imageurl:{
      type : String ,
      value : '../../../icon/error.png' // 默认值
    } 
  },

  /**
   * 组件内私有数据
   */
  data: {
    // 弹窗显示控制
    isShow:true
  },

  /**
   * 组件的公有方法列表
   */
  methods: {
    //隐藏
    hideFrame(e){
      console.log('隐藏事件')
      this.setData({
        isShow: !this.data.isShow
      })
      return false
    },
    //展示
    showDialog(){
      this.setData({
        isShow: !this.data.isShow
      })
    },
     /**
     * triggerEvent 组件之间通信
     */
    getUserProfile(e){
      this.hideFrame()
      this.triggerEvent("getUserProfile",e.detail,{});
    },
  }
})
