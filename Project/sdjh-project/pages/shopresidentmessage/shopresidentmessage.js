import {
  request
} from "../../request";
import {
  regeneratorRuntime
} from "../../libs/runtime/runtime";
//引入第三方表单审核插件
import WxValidate from '../../utils/WxValidate.js'
Page({
  //初始化数据
  data: {
    formData: {
      shop_username: '',
      shop_password: '',
    },
    ondisplay: 'none',
    defaultType: true,
    passwordType: true
  },
  onLoad() {
    this.initValidate() //验证规则函数
    this.sX();
  },
  //表单提交按钮
  formSubmit: function (e) {
    console.log('form发生了submit事件，携带数据为：', e.detail.value)
    const params = e.detail.value
    //校验表单
    if (!this.WxValidate.checkForm(params)) {
      const error = this.WxValidate.errorList[0]
      this.showModal(error)
      return false
    }
    this.getShopData(e.detail.value)
  },
  //表单重置按钮
  formReset: function (e) {
    console.log('form发生了reset事件')
    this.setData({
      formData: ''
    })
  },
  //上传账户密码
  async getShopData(e) {
    //使用async await发送异步请求
    console.log(e)
    const res = await request({
      url: '/shop_sc/zhuce',
      data: e
    });
    console.log(res)
    if (res === '登入成功') {
      const {
        shop_username,
        shop_password
      } = e
      wx.navigateTo({
        url: `/pages/shopresidentupload/shopresidentupload?shop_username=${shop_username}&shop_password=${shop_password}`,
      })
      this.setData({
        formData: ''
      })
    }else if(res === '账号密码出错'){
      wx.showModal({
        content: res,
        showCancel: false,
      })
    }
  },
  //表单报错提示 
  showModal(error) {
    wx.showModal({
      content: error.msg,
      showCancel: false,
    })
  },
  //表单验证函数
  initValidate() {
    const rules = {
      shop_username: {
        required: true,
        rangelength: [5, 10]
      },
      shop_password: {
        required: true,
        minlength: 6
      }
    }
    const messages = {
      shop_username: {
        required: '请填写账户',
        rangelength: '请输入长度为5-10的账户名称'
      },
      shop_password: {
        required: '请填写密码',
        minlength: '密码的最小长度为6位'
      }
    }
    this.WxValidate = new WxValidate(rules, messages)
  },
  async sX() {
    const res = await request({
      url: '/shuju/sx',
    });
    if (res.data === 1) {
      this.setData({
        ondisplay: 'flex'
      })
    }
  },
  //defaultType：眼睛状态   passwordType：密码可见与否状态
  eyeStatus: function() {
    if (this.data.defaultType) {
      this.setData({
        passwordType: false,
        defaultType: false,
      })
    } else {
      this.setData({
        passwordType: true,
        defaultType: true,
      })
    }
  },
})