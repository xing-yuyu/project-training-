import {
  request
} from "../../request";
import {
  regeneratorRuntime
} from "../../libs/runtime/runtime";
var startX, endX;
var moveFlag = true; // 判断执行滑动事件
Page({
  data: {
    currentindex: 0,
    userInfo: {},
    hasUserInfo: false,
    user: wx.getStorageSync('user'),
    note1: [],
    note2: [],
    collect1: [],
    collect2: [],
    NavTab: ['我发布的', '收藏'],
    Mineinfo: {},
    shop_ruzhu: '入驻商家',
    ondisplay: 'none',
    ondisplay1: 'none',
    dis: ''
  },
  // 触摸开始事件
  touchStart: function (e) {
    startX = e.touches[0].pageX;
    moveFlag = true;
  },
  // 触摸移动事件
  touchMove: function (e) {
    endX = e.touches[0].pageX;
    let index = this.data.currentindex
    if (moveFlag) {
      if (endX - startX > 200) {
        console.log("move right");
        if (index == 1)
          this.cgeNavtab(index - 1);
        else
          this.cgeNavtab(index + 1);
        moveFlag = false;
      }
      if (startX - endX > 200) {
        console.log("move left");
        if (index == 1)
          this.cgeNavtab(index - 1);
        else
          this.cgeNavtab(index + 1);
        moveFlag = false;
      }
    }
  },
  // 触摸结束事件
  touchEnd: function (e) {
    moveFlag = true; // 回复滑动事件
  },
  ChangeNavtab: function (e) {
    if (wx.getStorageSync('userInfo')) {
      let index = e.currentTarget.dataset.index
      this.cgeNavtab(index);
    } else {
      wx.showToast({
        title: '您还没有登录',
        image: '../../icon/error.png',
      })
      console.log('用户尚未登录')
    }
  },
  cgeNavtab: function (index) {
    let likeFlag = false;
    if (likeFlag === true) {
      return false;
    }
    this.setData({
      currentindex: index
    })
    if (index == 0)
      this.getnotedata()
    else
      this.getnotedata1()
  },
  onShow: function () {
    if (wx.getStorageSync('userInfo')) {
      this.setData({
        userInfo: wx.getStorageSync('userInfo'),
        hasUserInfo: true
      })
      if (this.data.currentindex == 0)
        this.getnotedata()
      else
        this.getnotedata1()
      this.GetMineinfo();
      this.Getredpoint();
      this.sX();
    }
  },
  onLoad: function () {
    if (wx.getStorageSync('userInfo')) {
      this.setData({
        userInfo: wx.getStorageSync('userInfo'),
        hasUserInfo: true
      })
      this.getnotedata();
      this.GetMineinfo();
      this.Getredpoint();
      this.sX();
    }
  },
  async Getredpoint() {
    const res = await request({
      url: '/wd/xx/hong',
      data: {
        user_id:wx.getStorageSync('openid'),
      }
    })
    this.setData({
      redpoint: res[0].hong
    })
    console.log(res)
  },
  async GetMineinfo() {
    const res = await request({
      url: '/yonghu',
      data: {
        user_id:wx.getStorageSync('openid')
      }
    })
    this.setData({
      Mineinfo: res[0]
    })
    console.log(res)
  },
  jumptodav: function (e) {
    wx.navigateTo({
      url: `/pages/${e.currentTarget.dataset.index}/${e.currentTarget.dataset.index}`,
    })
  },
  jumptomessage: function () {
    wx.navigateTo({
      url: '/pages/message/message',
    })
  },
  // 请求发布数据
  async getnotedata() {
    const res = await request({
      url: '/getwd',
      method:'POST',
      data: {
        openid:wx.getStorageSync('openid')
      }
    });
    let img3 = [];
    let img4 = [];
    console.log(res)
    for (let i in res) {
      if (i % 2 === 0)
        img3.push(res[i])
      else
        img4.push(res[i])
    }
    this.setData({
      note1: img3,
      note2: img4
    });
  },
  //请求收藏数据
  async getnotedata1() {
    const openid = wx.getStorageSync('openid')
    const res = await request({
      url: '/concern/user_collect',
      data: {
        user_id:openid,
      }
    });
    console.log(res)
    let img3 = [];
    let img4 = [];
    for (let i in res) {
      if (i % 2 === 0)
        img3.push(res[i])
      else
        img4.push(res[i])
    }
    this.setData({
      note11: img3,
      note22: img4
    });
  },
  jumpdetail1: function (e) {
    console.log(id);
    wx.navigateTo({
      url: `/pages/detail1/detail1?idx=${e.currentTarget.dataset.id}`,
    })
  },
  jumptoshop: function (e) {
    wx.navigateTo({
      url: `/pages/shopresidenttips/shopresidenttips`,
    })
  },
  jumptoshopinfor: function (e) {
    wx.navigateTo({
      url: `/pages/details3/details3?idx=${wx.getStorageSync('openid')}`,
    })
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
    // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log('getUserProfile获取到的用户信息')
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
        wx.setStorageSync('userInfo', res.userInfo);
        this.setData({
          userInfo: wx.getStorageSync('userInfo'),
          hasUserInfo: true
        })
        if (this.data.currentindex == 0)
          this.getnotedata()
        else
          this.getnotedata1()
        this.GetMineinfo();
        this.Getredpoint();
        wx.request({
          method: 'POST',
          url: `https://fourgoldhua.cn/aes-test/${wx.getStorageSync('openid')}`,
          header: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          data: {
            username: wx.getStorageSync('userInfo').nickName,
            avatarUrl: wx.getStorageSync('userInfo').avatarUrl
          },
          success: (res) => {
            console.log(wx.getStorageSync('userInfo').nickName)
          },
          fail:()=>{
            console.log('上传个人信息')
          }
        })
      },
    })
  },
  async sX() {
    const res = await request({
      url: '/shuju/sx',
    });
    if (res.data === 1) {
      const res1 = await request({
        url: '/shop_sc/open_id',
        data: {
          open_id: wx.getStorageSync('openid')
        }
      })
      if(res1 == 99){
        this.setData({
          ondisplay: 'flex',
          ondisplay1: 'none',
        })
      } else if(res1 == 0){
        this.setData({
          ondisplay: 'flex',
          ondisplay1: 'none',
          shop_ruzhu: '审核中',
          dis: 'true'
        })
      }  else {
        this.setData({
          ondisplay: 'none',
          ondisplay1: 'flex'
        })
      }
    }
  },
})