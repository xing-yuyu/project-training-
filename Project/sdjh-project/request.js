export const request = (params) => {
    //显示加载中
    wx.showLoading({
        title: '加载中',
        mask: true
    })
    //定义公共url 
    const baseUrl = 'https://fourgoldhua.cn'
    return new Promise((resolve, reject) => {
        wx.request({
            ...params,
            url: baseUrl + params.url,
            header:{
                token:`${wx.getStorageSync('openid')}`
            },
            success: (result) => {
                resolve(result.data);
            },
            fail: (err) => {
                reject(err);
            },
            complete: () => {
                wx.hideLoading();
            }
        })
    })
}