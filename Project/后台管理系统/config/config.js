import { Redirect } from "umi";

export default {
    singular: true,
    dva: {},
    antd: {},
    routes: [
        {path:'/login',component:'../layout/Login'},
        {
        path: '/',
        // redirect:'/helloworld',
        wrappers: [
            '../layout/Authorized',
        ],
        component: '../layout',
        routes: [
            {
                path: '/helloworld',
                component: 'HelloWorld'
            },
            {
                path: '/dashboard',
                routes: [
                    { path: '/dashboard/analysis', component: 'Dashboard/Analysis' },
                    { path: '/dashboard/monitor', component: 'Dashboard/Monitor' },
                    { path: '/dashboard/unreviewed', component: 'Dashboard/Unreviewed' },
                    { path: '/dashboard/xiangqing', component: 'Dashboard/xiangqing' },
                    { path: '/dashboard/weitongguo', component: 'Dashboard/Weitongguo' },
                ]
            },
            {
                path: '/store',
                component: 'Store/Message'
            },
            {
                path:'/store/change',
                component:'Store/Change'
            },
            {
                path:'/store/add',
                component:'Store/Add'
            },
            {
                path:'/comment',
                component:'Comment'
            },
            {
                path: '/manager',
                component: 'Manager'
            },
            {
                path:'/addmanager',
                component:'AddMan'
            }
        ]

    }],

};