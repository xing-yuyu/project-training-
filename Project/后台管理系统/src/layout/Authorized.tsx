import React from 'react';
const { Redirect } = require('dva').router;

const AuthRouter = (props:any) => {
  // 这个根据自己判断是否登录
  const isLogin = window.localStorage.getItem('username')?true:false;
  // const isLogin = window.localStorage.getItem('roles')?true:false;
  return (
  isLogin ? <div>{props.children}</div>: <Redirect to="/login" />
  )
}

export default AuthRouter;