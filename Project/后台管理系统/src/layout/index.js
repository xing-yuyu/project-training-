// 注意这里我们除了从antd中引入了Layout布局组件，还引入了Menu菜单组件，Icon图标组件
// import Link from 'umi/link';
import { Link, Redirect } from 'umi';
import { Component } from 'react';
import { Layout, Menu, Icon } from 'antd';
import { SmileOutlined } from '@ant-design/icons';


const { Header, Footer, Sider, Content } = Layout;

// 引入子菜单组件
const SubMenu = Menu.SubMenu;

export default class BasicLayout extends Component {
  constructor(){
    super()

    if(localStorage.getItem('key')){
      this.state={
        arr:[localStorage.getItem('key')]
      }
    }else{
      this.state={
        arr:['1']
      }
    }
  }
  handleClick=(item, key, selectedKeys)=>{
    console.log(item.key)
    localStorage.setItem('key',item.key)
    this.setState({
      arr:item.key
    })
  }
  componentWillUnmount(){
    localStorage.removeItem('key')
  }
  render() {
    return (
      <Layout>
        <Sider width={256} style={{ minHeight: '100vh' }}>
          <div style={{ height: '32px', background: 'rgba(255,255,255,.2)', 
          margin: '16px',color:'white' , textAlign: 'center',
          lineHeight: 2}}>
              你的倾心 我的用心
          </div>
          <Menu theme="dark" mode="inline" defaultSelectedKeys={this.state.arr} 
          onSelect={(item, key, selectedKeys)=>{this.handleClick(item, key, selectedKeys)}}>
            <Menu.Item key="1" >
              <Link to="/helloworld">
                <span>主页</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="2"><Link to="/dashboard/analysis">用户信息</Link></Menu.Item>
            <SubMenu
              key="sub1"
              title={<span><span>动态管理</span></span>}
            >
              <Menu.Item key="3"><Link to="/dashboard/monitor">审核通过</Link></Menu.Item>
              <Menu.Item key="4"><Link to="/dashboard/unreviewed">待审核</Link></Menu.Item>
              <Menu.Item key="5"><Link to="/dashboard/weitongguo">审核未通过</Link></Menu.Item>
            </SubMenu>
            <Menu.Item key="6">
              <Link to="/comment">
                <span>评论管理</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="7">
              <Link to="/store">
                <span>商户管理</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="8">
              <Link to="/manager">
                <span>系统管理</span>
              </Link>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout >
          <Header style={{ background: '#fff', textAlign: 'center', padding: 0 ,letterSpacing: 16,
          fontSize: 23,fontWeight:600}}>
            小食潭记后台管理系统
          </Header>
          <Content style={{ margin: '24px 16px 0' }}>
            <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
              {this.props.children}
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>小食潭记 2020 Designed by fourgoldhua</Footer>
        </Layout>
      </Layout>
    )
  }
}