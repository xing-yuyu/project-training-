import React, { Component } from 'react'
import { Form, Input, Button, message } from 'antd';
import { connect } from 'dva';
import { history } from 'umi';
import './login.css'

const namespace = 'login';

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};

const mapStateToProps = (state) => {
    return {
        a: 123
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        // 展示
        onDidMount: (mess) => {
            // console.log(mess)
            dispatch({
                type: `${namespace}/queryLogin`,
                payload: mess
            })
        },

    };
};

// @connect(() => ({}))
@connect(mapStateToProps, mapDispatchToProps)

export default class Login extends Component {
    onFinish = (e) => {
        this.props.onDidMount(e);
        setTimeout(() => {
            console.log(localStorage.getItem('username'));
            if (localStorage.getItem('username')) {
                message.success('登录成功')
                history.push('/helloworld');
            } else {
                message.error('用户名或密码错误！')
            }
        }, 1000)


    }

    render() {
        return (

            <div className='box'>
                <img src='http://fourgoldhua.cn:3001/public/xiaoshi.jpg' className='xstj'></img>
                <Form
                    {...layout}
                    name="login"
                    initialValues={{ remember: true }}//表单默认值
                    onFinish={this.onFinish}
                // onFinishFailed={onFinishFailed}
                >
                    <Form.Item
                        // label="登录名"
                        name="username"
                        className='fchild'
                        rules={[{ required: true, message: '请输入用户名!' }]}
                    >
                        <Input placeholder='登录名' />
                    </Form.Item>

                    <Form.Item
                        // label="密码"
                        name="password"
                        className='fchild'
                        rules={[{ required: true, message: '请输入密码!' }]}
                    >
                        <Input.Password placeholder='密码' />
                    </Form.Item>

                    {/* <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                        <Checkbox>Remember me</Checkbox>
                    </Form.Item> */}

                    <Form.Item {...tailLayout}>
                        <Button type="primary" htmlType="submit" className='btn'>
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        )
    }
}
