import request from '../util/request'

export default {
    namespace: 'getWeitongguo',
    state: {
        data: [],
        // counter: 0,
    },
    effects: {
        *queryInitCards(_, sagaEffects) {
            const { call, put } = sagaEffects;
            const endPointURI = 'http://fourgoldhua.cn:2000/shenhe?judge=2';
            const puzzle = yield call(request, endPointURI);
            yield put({ type: 'addNewCard', payload: puzzle });

            // yield call(delay, 3000);

            // const puzzle2 = yield call(request, endPointURI);
            // yield put({ type: 'addNewCard', payload: puzzle2 });
        }
    },
    reducers: {
        addNewCard(state, { payload: newCard }) {
            // const nextCounter = state.counter + 1;
            // const newCardWithId = { ...newCard, id: nextCounter };
            // const nextData = state.data.concat(newCardWithId);
            // console.log(nextData);
            console.log(newCard);
            return {
                data: newCard,
                // counter: nextCounter,
            };
        }
    },
};