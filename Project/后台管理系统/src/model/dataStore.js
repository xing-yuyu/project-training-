import request from '../util/request';

const delay = (millisecond) => {
    return new Promise((resolve) => {
        setTimeout(resolve, millisecond);
    })
};

// 获取商铺数据
const getRandomPuzzle = () => {
    return request('http://fourgoldhua.cn:2000/shop');
}

//删除数据
const getDelPuzzle = (id) => {
    console.log(id);
    return request('http://fourgoldhua.cn:2228/shopdel', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        },
        body:JSON.stringify({
            id:id
        })

    });
}

// 修改数据
const getchangePuzzle = (payload)=>{
    console.log(payload);
    let id = payload.data1.id,
    value = payload.values;
    value.id = id;
    console.log(value.shopimg);
    value.shopimg = value.shopimg.file.thumbUrl
    console.log(value)//传给接口的数据
    return request('http://fourgoldhua.cn:3006/changesp',{
        method:'POST',
        headers:{
            'Content-Type': 'application/json; charset=utf-8'
        },
        body:JSON.stringify(value)
    })

}

//搜索数据
const getSearchPuzzle = (value)=>{
    console.log(value);
    return request('http://fourgoldhua.cn:2000/shop/sousuo',{
        method:'POST',
        headers:{'Content-Type': 'application/json; charset=utf-8'},
        body:JSON.stringify({
            text:value
        })

    })
}

export default {
    namespace: 'dataStore',
    state: {
        data: [],
        // counter: 0
    },
    effects: {
        // 获取商家数据
        *queryInitCards(_, sagaEffects) {
            const { call, put } = sagaEffects;

            const puzzle = yield call(getRandomPuzzle);
            console.log(puzzle);
            yield put({ type: 'addNewCard', payload: puzzle });
            //yield put派发的action如果是为了触发同model中的其他 effect/reducer 执行,不需要指定namespace名称。

            yield call(delay, 3000);

            // const puzzle2 = yield call(getRandomPuzzle);
            // yield put({ type: 'addNewCard', payload: puzzle2 });
        },
        // 删除商家数据
        *queryDelCards({payload}, sagaEffects) {
            console.log(payload);
            const { call, put } = sagaEffects;
            const puzzle = yield call(getDelPuzzle,payload);
            console.log(puzzle)
            yield put({ type: 'addNewCard', payload:puzzle});
            yield call(delay, 3000);
        },
        // 修改商家数据
        *queryChangeData({payload},sagaEffects){
            console.log(payload);
            const { call, put } = sagaEffects;
            const puzzle = yield call(getchangePuzzle,payload);
            console.log(puzzle);
            yield call(delay,3000)
        },
        //搜索商家数据
        *querySearchCards({payload},sagaEffects){
            console.log(payload);
            const { call, put } = sagaEffects;
            const puzzle = yield call(getSearchPuzzle,payload);
            console.log(puzzle);
            yield put({ type: 'searchStoreDate', payload:puzzle});
            yield call(delay, 3000);

        }
        //
    },
    reducers: {
        addNewCard(state, { payload: newCard }) {
            // const nextCounter = state.counter + 1;
            // const newCardWithId = { ...newCard, id: nextCounter };
            console.log(newCard);
            // const newCardWithId = {newCard};
            // const nextData = state.data.concat(newCardWithId);
            // console.log(newCard)
            return {
                data: newCard,
            };
        },
        delStoreData(state,{payload:newData}) {
            console.log(newData)
            const copyState = Object.assign({},state,{
                data:newData
            })
            return{
                copyState
            }
        },
        searchStoreDate(state,{payload:newData}){
            console.log(newData);
            // const searchData = Object.assign({},state.{
            //     dataSearch:newData
            // });
            return {
                data: newData
            }
        }
    },
}