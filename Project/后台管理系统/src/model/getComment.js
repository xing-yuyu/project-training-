import request from '../util/request';
import queryString from 'query-string';

const delay = (millisecond) => {
    return new Promise((resolve) => {
        setTimeout(resolve, millisecond);
    })
};

//获取评论
const getCom = () => {
    return request('http://fourgoldhua.cn:2000/pinglun', {
        method: 'GET',
    });
}
//删除评论，万事俱备只欠跨域
const delCom = (id)=>{
    console.log(id)
    return request('http://fourgoldhua.cn:2000/delpinglun', {
        method: 'POST',
        headers:{'Content-Type': 'application/json; charset=utf-8'},
        body:JSON.stringify({
            id:id
        })
    });
}

export default {
    namespace: 'getComment',
    state: {
        data:[]
    },
    effects: {
        // 获取评论数据
        *queryAdd({payload}, sagaEffects) {
            const { call, put } = sagaEffects;
            // console.log(payload);
            const puzzle = yield call(getCom);
            console.log(puzzle);
            yield put({ type: 'getData', payload:puzzle});
            yield call(delay, 3000);
        },
        *queryDel({payload}, sagaEffects) {
            const { call, put } = sagaEffects;
            console.log(payload);
            const puzzle = yield call(delCom,payload);
            console.log(puzzle);
            yield put({ type: 'getData', payload:puzzle});
            yield call(delay, 3000);
        }
    },
    reducers: {
        getData(state,{payload:newData}){
            console.log(newData);
            return {
                data: newData
            }
        },
        
    },
}