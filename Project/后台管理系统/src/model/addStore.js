import request from '../util/request';
import queryString from 'query-string';

const delay = (millisecond) => {
    return new Promise((resolve) => {
        setTimeout(resolve, millisecond);
    })
};

const getAdd = (mess) => {
    console.log(mess)
    return request('http://fourgoldhua.cn:3006/upshop', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        },
        body:JSON.stringify({
            shopname:mess.shopname,
            shopaddr:mess.shopaddr,
            shopscount:mess.shopscount,
            shoplabel:mess.shoplabel,
            shopmid:'1027434691792581',
            shoptel:mess.shoptel,
            shopwm:mess.shopwm,
            shopimg:mess.shopimg.file.thumbUrl,
            shopjw:mess.shopmjw,
            shopcount:mess.shopcount,
            shopdistribution1:mess.shopdistribution1,
            shopdistribution2:mess.shopdistribution2,
            shopreduction1:mess.shopreduction1,
            shopreduction2:mess.shopreduction2,
            shopreduction3:mess.shopreduction3
        })

    });
}

export default {
    namespace: 'addstore',
    state: {
        
    },
    effects: {
        // 上传商家数据
        *queryAdd({payload}, sagaEffects) {
            const { call, put } = sagaEffects;
            console.log(payload);
            const puzzle = yield call(getAdd,payload);
            console.log(puzzle);
            return puzzle;
        },
    },
    reducers: {

    },
}