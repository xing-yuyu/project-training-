import request from '../util/request';
import queryString from 'query-string';

const delay = (millisecond) => {
    return new Promise((resolve) => {
        setTimeout(resolve, millisecond);
    })
};

//获取评论
const getUser = () => {
    return request('http://fourgoldhua.cn:2000/yonghu', {
        method: 'GET',
    });
}

export default {
    namespace: 'userStore',
    state: {
        data:[]
    },
    effects: {
        // 获取评论数据
        *querygetUser({payload}, sagaEffects) {
            const { call, put } = sagaEffects;
            // console.log(payload);
            const puzzle = yield call(getUser);
            console.log(puzzle);
            yield put({ type: 'getData', payload:puzzle});
            yield call(delay, 3000);
        },
    },
    reducers: {
        getData(state,{payload:newData}){
            console.log(newData);
            return {
                data: newData
            }
        },
        
    },
}