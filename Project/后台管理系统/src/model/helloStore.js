import request from '../util/request';
import queryString from 'query-string';

const delay = (millisecond) => {
    return new Promise((resolve) => {
        setTimeout(resolve, millisecond);
    })
};

//获取评论
const getHello = () => {
    return request('http://fourgoldhua.cn:2000/shouye/shuju', {
        method: 'GET',
    });
}

export default {
    namespace: 'helloStore',
    state: {
        data:[]
    },
    effects: {
        // 获取评论数据
        *querygetHello({payload}, sagaEffects) {
            const { call, put } = sagaEffects;
            // console.log(payload);
            const puzzle = yield call(getHello);
            console.log(puzzle);
            yield put({ type: 'getData', payload:puzzle});
            yield call(delay, 3000);
        },
    },
    reducers: {
        getData(state,{payload:newData}){
            console.log(newData);
            return {
                data: newData
            }
        },
        
    },
}