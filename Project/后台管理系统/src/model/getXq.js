import request from '../util/request'

// const delay = (millisecond) => {
//     return new Promise((resolve) => {
//         setTimeout(resolve, millisecond);
//     });
// };
const getxq = (id) => {
    console.log(id);
    return request('http://fourgoldhua.cn:2000/xiangqing',{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        },
        body:JSON.stringify({
            id:id
        })
    })
}

const changestatus = (ids) => {
    console.log(ids);
    
    return request('http://fourgoldhua.cn:2000/shenhezhong',{
        method:'POST',
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        },
        body:JSON.stringify({
            share_id: ids.id,
            judge: ids.status1
        })
    })
}

export default {
    namespace: 'getXq',
    state: {
        data: [],
        havedata:false,
        // counter: 0,
    },
    effects: {
        *queryInitCards({payload}, sagaEffects) {
            // console.log(payload)
            const { call, put } = sagaEffects;
            const puzzle = yield call(getxq, payload);
            yield put({ type: 'addXiangQing', payload: puzzle });
        },
        *queryInitStatus({payload}, sagaEffects) {
            console.log(payload);
            const { call, put } = sagaEffects;
            const puzzle1 = yield call(changestatus,payload);
            yield put({type: 'changeStatus',payload: puzzle1});
        }
    },
    reducers: {
        addXiangQing(state, { payload: newCard }) {
            console.log(newCard);
            return {
                data: newCard,
                havedata:true,
            };
        },
        changeStatus(state, {payload: success}) {
            console.log(success);
            return {
                data: success
            }
        }
    },
};