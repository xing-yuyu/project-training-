import request from '../util/request';

const delay = (millisecond) => {
    return new Promise((resolve) => {
        setTimeout(resolve, millisecond);
    })
};
// 验证登录
const getLogin = (mess) => {
    return request('http://fourgoldhua.cn:2001/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        },
        body:JSON.stringify({
            username:mess.username,
            password:mess.password
        })

    });
}
// 获取管理员数据
const getLoginData = () => {
    return request('http://fourgoldhua.cn:2001/login/get', {
        method: 'POST',
    });
}
//修改
const changeData = (payload)=>{
    console.log(payload)//传给接口的数据
    return request('http://fourgoldhua.cn:3006/changesp',{
        method:'POST',
        headers:{
            'Content-Type': 'application/json; charset=utf-8'
        },
        body:JSON.stringify(payload)
    })

}
// 删除
const delData = (id) => {
    console.log(id);
    return request('http://fourgoldhua.cn:2001/logindel', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        },
        body:JSON.stringify({
            id:id
        })

    });
}
// 添加
const addData = (value)=>{
    console.log(value)
    return request('http://fourgoldhua.cn:2001/logininsert', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        },
        body:JSON.stringify({
            username:value.user_name,
            password:value.password
        })

    });
}

export default {
    namespace: 'login',
    state: {
        data: [],
        // counter: 0
    },
    effects: {
        // 验证登录
        *queryLogin({payload}, sagaEffects) {
            const { call, put } = sagaEffects;

            const puzzle = yield call(getLogin,payload);
            console.log(puzzle);
            if(puzzle == '成功'){
                localStorage.setItem('username',payload.username);
                localStorage.setItem('password',payload.password);
                
            }
            // console.log(localStorage.getItem('username'));
            // yield put({ type: 'loginCard', payload: puzzle });
            //yield put派发的action如果是为了触发同model中的其他 effect/reducer 执行,不需要指定namespace名称。
            yield call(delay, 3000);
            
        },
        //获取管理员数据
        *queryLoginData({payload}, sagaEffects) {
            const { call, put } = sagaEffects;
            // console.log(payload);
            const puzzle = yield call(getLoginData);
            console.log(puzzle);
            yield put({ type: 'getData', payload:puzzle});
            yield call(delay, 3000);
        },
        //修改管理员数据
        *querySave({payload}, sagaEffects) {
            const { call, put } = sagaEffects;
            // console.log(payload);
            const puzzle = yield call(changeData);
            console.log(puzzle);
            // yield put({ type: 'getData', payload:puzzle});
            yield call(delay, 3000);
        },
        //删除数据
        *queryDel({payload}, sagaEffects){
            const { call, put } = sagaEffects;
            console.log(payload);
            const puzzle = yield call(delData,payload);
            console.log(puzzle);
            yield put({ type: 'getData', payload:puzzle});
            yield call(delay, 3000);
        },
        // 添加数据
        *queryAdd({payload}, sagaEffects){
            const { call, put } = sagaEffects;
            console.log(payload);
            const puzzle = yield call(addData,payload);
            // console.log(puzzle);
            // yield put({ type: 'getData', payload:puzzle});
            yield call(delay, 3000);
        }
    },
    reducers: {
        getData(state,{payload:newData}){
            console.log(newData);
            return {
                data: newData
            }
        },
    },
}