import React, { Component } from 'react'
import { Link } from 'umi'
import { connect } from 'dva';
import { Form, Input, Button,Upload,Checkbox } from 'antd';
import { message,Space } from 'antd';


const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};
const namespace = 'login';

const mapStateToProps = (state) => {
    return{
        a:123
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        // 展示
        onAddMount: (mess) => {
            // console.log(mess.shopimg.file.thumbUrl)
            dispatch({
                type: `${namespace}/queryAdd`,
                payload: mess
            })
        },

    };
};

@connect(mapStateToProps, mapDispatchToProps)

export class AddMan extends Component {
    onFinish =async values => {
        console.log(values);
        await this.props.onAddMount(values);
        message.success('添加成功！')
        // await message.success('添加成功！',1);
        location.replace("http://localhost:8000/manager")
    };

    render() {
        return (
            <div>
                <Link to='/manager'>返回</Link>
                <Form {...layout} name="basic"
                    onFinish={this.onFinish}
                >
                    <Form.Item
                        label="管理员"
                        name="user_name"
                        rules={[{ required: true, message: '请输入管理员名!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="管理密码"
                        name="password"
                        rules={[{ required: true, message: '请输入管理员密码' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <Button type="primary" htmlType="submit">
                            提交
                        </Button>
                    </Form.Item>

                </Form>
            </div>
        )
    }
}

export default AddMan