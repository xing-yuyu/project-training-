import React, { Component } from 'react'
import { Link } from 'umi'
import { connect } from 'dva';
import { useContext, useState, useEffect, useRef } from 'react';
import { Table, Input, Button, Popconfirm, Form } from 'antd';


const namespace = 'login';

const mapStateToProps = (state) => {
  const userList = state[namespace].data;
  return {
    userList
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    // 展示数据
    onDidMount: () => {
      dispatch({
        type: `${namespace}/queryLoginData`,
      })
    },
    //删除数据
    delComment: (value) => {
      console.log(value.id)
      dispatch({
        type: `${namespace}/queryDel`,
        payload: value.id
      })
    },
    //保存数据
    saveComment:(value) => {
      console.log(value);
      dispatch({
        type: `${namespace}/querySave`,
        payload: value.id
      })
    }
  }
}
const style = {
  color:'red',
  fontSize:'2em',
  fontWeight:'600'
}

@connect(mapStateToProps, mapDispatchToProps)

class Manager extends React.Component {
  constructor(props) {
    super(props);
    //表头
    this.columns = [
      {
          title: '管理员id',
          dataIndex: 'id',
          key: 'id',
      },
      {
          title: '管理员名',
          dataIndex: 'user_name',
          key: 'user_name',
      },
      {
          title: '管理密码',
          dataIndex: 'password',
          key: 'password',
      },

      {
          title: 'Action',
          key: 'action',
          render: (e) => (
            <div>
              <Popconfirm title="确定删除该数据?" onConfirm={() => this.props.delComment(e)}>
                  <a>删除</a>
              </Popconfirm>
            </div>
              
          ),
      },
  ];
}
  //获取数据
  componentDidMount() {
    this.props.onDidMount();
    setTimeout(() => {
      this.setState({ loading: false })
    }, 1500)
  };

  render() {
    let name = localStorage.getItem('username');
    if(name!=='root'){
      return <p style={style}>对不起，您无权限访问该页面！</p>
    }
    return (
      <div>
        <Button
          onClick={this.handleAdd}
          type="primary"
          style={{
            marginBottom: 16,
          }}
        >
          <Link to='/addmanager'>添加管理员</Link>
        </Button>
        <Table
          // bordered
          dataSource={this.props.userList}
          columns={this.columns}
        />
      </div>
    );
  }
}



// 设置表头
// const columns = [
//   {
//     title: 'Name',
//     dataIndex: 'name',
//     key: 'name',
//     render: text => <a>{text}</a>,
//   },
//   {
//     title: 'Password',
//     dataIndex: 'password',
//     key: 'password',
//   },
//   {
//     title: 'Action',
//     key: 'action',
//     render: (text, record) => (
//       <Space size="middle">
//         <a>Delete</a>
//       </Space>
//     ),
//   },
// ];

// //从服务端获取管理员的username和password
// const originData = [
//   {
//     key: '1',
//     name: 'John Brown',
//     password:'000000'
//   },
//   {
//     key: '2',
//     name: 'Jim Green',
//     password:'000000'
//   },
//   {
//     key: '3',
//     name: 'Joe Black',
//     password:'000000'
//   },
// ];


// const Manager = props => {
//   return (
//     <div>
//       <Table columns={columns} dataSource={originData} />
//     </div>
//   )
// }



export default Manager
