import React, { Component } from 'react';
import { List, Avatar, Space, Button, Image, Popconfirm, message, BackTop } from 'antd';
import { connect } from 'dva';
import queryList from './Analysis';
import { Skeleton } from 'antd';
import { Link } from 'umi';
import { UpOutlined } from '@ant-design/icons';

// import { MessageOutlined, LikeOutlined, StarOutlined } from '@ant-design/icons';
// Analysis.js
//动态管理

const namespace = 'getReviewed';

const mapStateToProps = (state) => {
  const ctList = state[namespace].data;
  return {
    ctList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onDidMount: () => {
      dispatch({
        type: `${namespace}/queryInitCards`,
      });
    },
  };
};

// const IconText = ({ icon, text }) => (
//   <Space>
//     {React.createElement(icon)}
//     {text}
//   </Space>
// );

const style1 = {
  position: 'relative',
  left: '60px',
  height: 40,
  width: 40,
  lineHeight: '40px',
  borderRadius: 4,
  backgroundColor: '#1088e9',
  color: '#fff',
  textAlign: 'center',
  fontSize: 14,
};

@connect(mapStateToProps, mapDispatchToProps)

export default class Monitor extends Component {
  constructor(props){
    super(props)
    this.state={
        loading:true
    }
  }
  //延迟1.5秒
  componentDidMount() {
    this.props.onDidMount();
    setTimeout(()=>{
      this.setState({loading : false})
    },1000)
  }
  
  componentWillUnmount = () => {
    this.setState = (state,callback)=>{ //解决刷新异步找不到state的问题，也可以清除所有异步来实现。
      return;
    };
  }
  render() {
    console.log(this.props.ctList);
    //创建数组，添加数据到数组中
    const listData1 = [];
    for (let i = 0; i < this.props.ctList.length; i++) {
      if(this.props.ctList[i].title == ''){
        this.props.ctList[i].title = '该用户默认无标题'
      }
      if(this.props.ctList[i].text == ''){
        this.props.ctList[i].text = '该用户默认无内容'
      }
      listData1.push({
        img: `http://fourgoldhua.cn:3001${this.props.ctList[i].img1}`,
        shareid: `${this.props.ctList[i].share_id}`,
        href: 'https://ant.design',
        title: `${this.props.ctList[i].user_name}`,//用户名
        avatar: `${this.props.ctList[i].user_img}`,//头像
        //标题
        description:
          `${this.props.ctList[i].title}`,
        //内容
        content:
          `${this.props.ctList[i].text}`,
      });
    }


    return (
      
      <List
        itemLayout="vertical"
        size="large"
        pagination={{
          onChange: page => {
            console.log(page);
          },
          pageSize: 4,
        }}
        dataSource={listData1}
        footer={
          <div>
            <b>小食潭记</b> 已审核页面
          </div>
        }
        renderItem={item => (
          <List.Item

            key={item.shareid}
            actions={[
              <Link to={`/dashboard/xiangqing?id=${item.shareid}`}>
                <Button type="link" className='lala'>Link</Button>
              </Link>
              
            ]}
            // actions={[
            //     <IconText icon={StarOutlined} text="156" key="list-vertical-star-o" />,
            //     <IconText icon={LikeOutlined} text="156" key="list-vertical-like-o" />,
            //     <IconText icon={MessageOutlined} text="2" key="list-vertical-message" />,
            // ]}
            extra={
              // 右侧图片
              <Skeleton  loading={this.state.loading}>
                
                <div style={{width: 272+'px',overflow: 'hidden',height: 180+'px'}}
                >
                  <img
                  style={{width: 100+'%'}}
                  alt="logo"
                  src={`${item.img}`}
                />
                </div>
              </Skeleton>
              
            }
          >

            <Skeleton title={false} loading={this.state.loading} active>
              <List.Item.Meta
                avatar={<Avatar src={item.avatar} />}
                title={<a href='/dashboard/xiangqing'>{item.title}</a>}
                description={item.description}
              />

              <div style={{'maxWidth': 800+'px','textOverflow': 'ellipsis',overflow: 'hidden','whiteSpace': 'nowrap'}}>
              {item.content}
              </div>
              
              
            </Skeleton>
            <BackTop>
              <div style={style1}><UpOutlined /></div>
            </BackTop>
          </List.Item>
          
        )}
      />
    )
    // mountNode
  }
}

