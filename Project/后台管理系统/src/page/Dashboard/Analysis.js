import React, { Component } from 'react'
import { connect } from 'dva';
import { Table, Input, Button, Popconfirm, Form } from 'antd';

const namespace = 'userStore';

const mapStateToProps = (state) => {
    const userList = state[namespace].data;
    return {
        userList
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        // 展示用户
        onDidMount: () => {
            dispatch({
                type: `${namespace}/querygetUser`,
            })
        },
    }
}


@connect(mapStateToProps, mapDispatchToProps)

export default class Analysis extends Component {
	constructor(props){
		super(props);
		this.columns = [
			{
                title: '用户名',
                dataIndex: 'user_name',
                key: 'user_name',
            },
			{
                title: '用户id',
                dataIndex: 'user_id',
                key: 'id',
            },
            {
                title: '关注人数',
                dataIndex: 'davnum',
                key: 'davnum',
            },
            {
                title: '粉丝数',
                dataIndex: 'fensinum',
                key: 'fensinum',
			},
			{
                title: '关注店铺',
                dataIndex: 'shopnum',
                key: 'shopnum',
            }
		]
	}

	componentDidMount() {
        this.props.onDidMount();
        setTimeout(() => {
            this.setState({ loading: false })
        }, 1500)
    };

	render() {
		return (
			<div>
				<Table columns={this.columns}
                    dataSource={this.props.userList} />
			</div>
		)
	}
}

 