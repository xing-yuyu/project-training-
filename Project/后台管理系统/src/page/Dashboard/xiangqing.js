import React, { Component } from 'react';
import { List, Avatar, Space, Button, Image, Popconfirm, message, BackTop } from 'antd'
import { connect } from 'dva';
import { Descriptions, Badge } from 'antd';
import queryList from './Analysis';
import { Skeleton } from 'antd';
import { Link } from 'umi';
import './xiangqing.css';

// import { MessageOutlined, LikeOutlined, StarOutlined } from '@ant-design/icons';
// Analysis.js
//动态管理


const namespace = 'getXq';

const mapStateToProps = (state) => {
  const xqList = state[namespace].data;
  const have1 = state[namespace].havedata;
  return {
    xqList, have1
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onDidMount: (id) => {
      dispatch({
        type: `${namespace}/queryInitCards`,
        payload: id
      });
    },
    onChange: (id, status1) => {
      dispatch({
        type: `${namespace}/queryInitStatus`,
        payload: { id, status1 }
        // payload2: status1
      });
    }
  };
};





@connect(mapStateToProps, mapDispatchToProps)

export default class Monitor extends Component {


  constructor(props) {
    super(props)
    // this.state={
    //     loading:true
    // }
    this.confirm1 = this.confirm1.bind(this);
    this.confirm2 = this.confirm2.bind(this);
    this.confirm3 = this.confirm3.bind(this);
  }
  //点击审核函数
  async confirm1(e) {

    await this.props.onChange(this.props.location.query.id, '1');
    message.success('状态改变，通过审核');
    location.reload();
  }
  async confirm2(e) {

    await this.props.onChange(this.props.location.query.id, '0');
    message.success('状态改变，重新审核');
    location.reload();
  }
  async confirm3(e) {

    await this.props.onChange(this.props.location.query.id, '2');
    message.success('状态改变，不通过审核');
    location.reload();
  }

  //延迟1.5秒
  componentDidMount() {
    console.log(this.props.location.query.id);
    this.props.onDidMount(this.props.location.query.id);
  }
  render() {

    // console.log(this.props.location.query.id);
    console.log(this.props.xqList[0]);
    var style = {
      color: 'rgb(0, 216, 0)',
      fontWeight: '600'
    }
    console.log(this.props);
    //审核通过函数

    function cancel1(e) {
      // console.log(e);
      message.error('状态未发生改变');
    }

    //审核不通过函数

    function cancel2(e) {
      // console.log(e);
      message.error('状态未发生改变');
    }

    function cancel3(e) {
      // console.log(e);
      message.error('状态未发生改变');
    }


    //由于是异步，需要判断数据
    //得到数据返回
    if (this.props.have1 == true) {
      var status;
      var dis1;
      var dis2;
      if (this.props.xqList[0].judge == '1') {
        status = '已通过审核';
        dis1 = true;
        dis2 = false;
      } else if (this.props.xqList[0].judge == '0') {
        status = '待审核';
        dis1 = false;
        dis2 = true;
        style.color = 'orange';
      } else if (this.props.xqList[0].judge == '2') {
        status = '未通过审核';
        dis1 = true;
        dis2 = true;
        style.color = 'red';
      }
      // console.log(Object.keys(this.props.xqList[0]).length);
      const lala = Object.values(this.props.xqList[0]);
      console.log(lala);
      const imgList = [];
      for (var i = 11; i < Object.keys(this.props.xqList[0]).length - 1; i++) {
        imgList[i - 11] = 'http://fourgoldhua.cn:3001' + lala[i];
      }
      console.log(imgList);
      return (
        <div>

          <Button onClick={() => { history.back() }}>返回</Button>

          <Descriptions title="User Info" bordered>
            <Descriptions.Item label={<div style={{ fontWeight: '900' }}>用户名</div>}>{this.props.xqList[0].user_name}</Descriptions.Item>

            <Descriptions.Item label={<div style={{ fontWeight: '900' }}>分享ID</div>}>{this.props.xqList[0].share_id}</Descriptions.Item>

            <Descriptions.Item label={<div style={{ fontWeight: '900' }}>餐食类型</div>}>{this.props.xqList[0].food}</Descriptions.Item>

            <Descriptions.Item label={<div style={{ fontWeight: '900' }}>上传时间</div>}>{this.props.xqList[0].date}</Descriptions.Item>
            <Descriptions.Item label={<div style={{ fontWeight: '900' }}>地址</div>} span={2}>
              {this.props.xqList[0].addr}
            </Descriptions.Item>
            <Descriptions.Item label={<div style={{ fontWeight: '900' }}>状态</div>} span={3}>
              <Badge status="processing" text={`${status}`} style={style} />
            </Descriptions.Item>
            <Descriptions.Item label={<div style={{ fontWeight: '900' }}>标题</div>}>{this.props.xqList[0].title}</Descriptions.Item>
            <Descriptions.Item label={<div style={{ fontWeight: '900' }}>标签</div>}>{this.props.xqList[0].tags}</Descriptions.Item>
            <Descriptions.Item label={<div style={{ fontWeight: '900' }}>推荐指数</div>}>{this.props.xqList[0].top}</Descriptions.Item>
            <Descriptions.Item label={<div style={{ fontWeight: '900' }}>内容</div>}>
              {this.props.xqList[0].text}
              <br />
              <br />
              <Image.PreviewGroup>
                {
                  imgList.map((item, index) => (
                    <div key={index} className='image'>
                      <Image

                        width={200}
                        src={item}
                      />
                    </div>
                  ))
                }
              </Image.PreviewGroup>
              <br />
            </Descriptions.Item>
          </Descriptions>
          <div className='shenhe'>

            <Button disabled={dis1}>
              <Popconfirm
                title="确定通过该审核吗？"
                onConfirm={this.confirm1}
                onCancel={cancel1}
                okText="确定"
                cancelText="取消"
              >
                通过审核
                </Popconfirm>
            </Button>
            
            <Button disabled={dis1}>
              <Popconfirm
                title="确定不通过该审核吗？"
                onConfirm={this.confirm3}
                onCancel={cancel3}
                okText="确定"
                cancelText="取消"
              >
                不通过审核
                </Popconfirm>
            </Button>

            <Button disabled={dis2}>
              <Popconfirm
                title="确定重新审核吗？"
                onConfirm={this.confirm2}
                onCancel={cancel2}
                okText="确定"
                cancelText="取消"
              >
                重新审核
                </Popconfirm>
            </Button>

            


          </div>

        </div>

      )
    } else {
      //未得到数据返回
      return (
        <Button type="primary" loading>
          Loading
        </Button>
      )
    }
  }
}


