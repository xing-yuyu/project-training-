import React, { Component } from 'react';
import { Skeleton } from 'antd';
import { Popconfirm, message } from 'antd';
import { Card ,Button  } from 'antd';
import { connect } from 'dva';
import { Collapse } from 'antd';
import { Descriptions, Badge } from 'antd';
import './Message.css'
import { List, Avatar, Space } from 'antd';
import { Link } from 'umi';
import { Select } from 'antd';
import {RedoOutlined} from '@ant-design/icons';

const { Panel } = Collapse;

const namespace = 'dataStore';

//action.type 的构造是 namespace 名称 + / + reducer 名称，或 
//也可以是 namespace 名称 + / + effect 名称

const mapStateToProps = (state) => {
  const storeList = state[namespace].data;
  const dataSearch1 = state[namespace].dataSearch1;
  return {
    storeList,
    dataSearch1
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    // 展示
    onDidMount: () => {
      dispatch({
        type: `${namespace}/queryInitCards`,
      })
    },

    // 删除
    delData: (id) => {
      dispatch({
        type: `${namespace}/queryDelCards`,
        payload: id
      })
    },
    //查询
    searchData:(value)=>{
      console.log(value);
      dispatch({
        type:`${namespace}/querySearchCards`,
        payload:value
      })
    }

  };
};

const style = {
  width: '100%',
  height: '90%',
  boxShadow: '0 0 9 3px gray',
  marginTop: '73px'
}

const deleteS = {
  marginRight:'15px'
}

const { Option } = Select;

@connect(mapStateToProps, mapDispatchToProps)

export default class Message extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      dataSearch:[],
      value:undefined
    }
  }

  handleChange = value => {
    console.log('点击'+value)
    this.setState({ value });

  };

  handleSearch = value =>{
    let arr = this.props.storeList;
    this.setState({
      dataSearch:[]
    })
    console.log(this.props.storeList)
    let data = arr.filter((item)=>{
      return item.shop_name.search(value) !=-1
    });
    this.setState({
      dataSearch:data
    });
  }

  reset=(value)=>{
    // this.props.onDidMount();
    let arr = this.props.storeList;
    let data = arr.filter((item)=>{
      return item.id == value
    });
    console.log(data)
    this.props.searchData(data[0].shop_name)
  }
   
  reload=()=>{
    location.reload()
  }

  componentDidMount() {
    this.props.onDidMount();
    setTimeout(() => {
      this.setState({ loading: false })
    }, 1500)
  };


  confirm(id) {
    // console.log(e);
    console.log(id);
    this.props.delData(id);
    message.success('删除成功');
    //刷新页面
    // location.replace("http://localhost:8000/store")
  };


  // console.log(this.props.storeList);
  render() {
    const pageSize = 10;
    console.log(this.props.storeList);
    var options = <Option></Option>;
    if(this.state.dataSearch !==[]){
      options = this.state.dataSearch.map(d => <Option key={d.id}>{d.shop_name}</Option>);
    }
    // const options = this.state.dataSearch.map(d => <Option key={d.id}>{d.shop_name}</Option>);
    return (
      <div>
        {/* 顶部条 */}
        <div className='top'>
          <span className='search'>搜索：</span>
          <Select
                showSearch//使单选模式可搜索
                value={this.state.value}//指定当前选中的条目
                placeholder={this.props.placeholder}//选择框默认文本
                style={this.props.style}
                defaultActiveFirstOption={false}//是否默认高亮第一个选项
                showArrow={false}//是否显示下拉小箭头
                filterOption={false}
                placeholder='请在此处搜索店铺'
                onSearch={this.handleSearch}//文本框值变化时回调
                onChange={this.handleChange}//选中 option，或 input 的 value 变化时，调用此函数
                onSelect={this.reset}
                notFoundContent='未找到该商铺'//当下拉列表为空时显示的内容
          >
            {options}

          </Select>
          <span onClick={this.reload}><RedoOutlined className='shauxin'/></span>
          
          <Link to='/store/add'><Button className='add' >添加</Button></Link>
        </div>

        {/* 商铺列表 */}
        <List
          itemLayout="vertical"
          size="large"
          pagination={{
            onChange: page => {
              console.log(page);
            },
            pageSize: 3,
          }}
          dataSource={this.props.storeList}
          renderItem={item => (
            <List.Item
              key={item.id}
              extra={
                <Skeleton loading={this.state.loading}>
                  <div style={{ width: 230 + 'px', overflow: 'hidden', height: 180 + 'px' }}>
                    <img
                      style={style}
                      alt="logo"
                      src={'http://fourgoldhua.cn:3001' + item.shop_img}
                    />
                  </div>
                </Skeleton>
              }
            >
              <Skeleton title={false} loading={this.state.loading} active>

                <List.Item.Meta
                  title={item.shop_name}
                  description={item.shop_label}
                />
                <Descriptions bordered>
                  <Descriptions.Item label="评分">{item.shop_starcount}</Descriptions.Item>
                  <Descriptions.Item label="电话" span={2}>{item.shop_tel}</Descriptions.Item>
                  <Descriptions.Item label="月销">{item.shop_count}</Descriptions.Item>
                  <Descriptions.Item label="地址" span={2}>
                    {item.shop_addr}
                  </Descriptions.Item>
                </Descriptions>
              </Skeleton>
              <br></br>
              <Popconfirm
                title="确定删除此商家信息?"
                onConfirm={()=>{this.confirm(item.id)}}
                okText="Yes"
                cancelText="No"
              >
                <a href="#" style={deleteS}>删除</a>
              </Popconfirm>
              <Link to={`/store/change?id=${item.id}`}>
                 编辑
              </Link>
              

            </List.Item>
          )}
        />
      </div>

    );
  }
}