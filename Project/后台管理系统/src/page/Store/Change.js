import React from 'react'
import { Component } from 'react'
import { Link } from 'umi'
import { connect } from 'dva';
import { Form, Input, Button,Upload } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { message,Space } from 'antd';

import './Change.css'



const namespace = 'dataStore';
const mapStateToProps = (state) => {
    const storeList = state[namespace].data;
    return {
        storeList,
    };
};
const mapDispatchToProps  = (dispatch)=>{
    return {
        changeData:(values)=>{
            let data1={};
            data1.id = localStorage.getItem('id')
            console.log(values);
            dispatch({
                type:`${namespace}/queryChangeData`,
                payload:{data1,values}
            })
        }
    }
}
// 表单
const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};

@connect(mapStateToProps, mapDispatchToProps)

export default class Change extends Component {
    constructor(props) {
        super(props);
        this.state = {
            shop: {}
        }
    }
    onFinish = async (values) => {
        console.log(values);
        console.log(localStorage.getItem('id'));
        // values.id = localStorage.getItem('id');
        // console.log(values);
        await this.props.changeData(values);
        message.success('修改成功！')
        setTimeout(()=>{
            console.log('sent');
            localStorage.removeItem('id');
            localStorage.removeItem('shopaddr');
            localStorage.removeItem('shopname');
            localStorage.removeItem('shopimg');
            localStorage.removeItem('shopmid');
            localStorage.removeItem('shopcount');
            localStorage.removeItem('shoptel');
            localStorage.removeItem('shopwm');
            localStorage.removeItem('shoplabel');
            localStorage.removeItem('shopjw')
            localStorage.removeItem('shopscount')
            localStorage.removeItem('shopdistribution1')
            localStorage.removeItem('shopdistribution2')
            localStorage.removeItem('shopreduction1')
            localStorage.removeItem('shopreduction2')
            localStorage.removeItem('shopreduction3');
            location.replace("http://localhost:8000/store"
        )},1000);
        
    };
    

    render() {
        let arr = this.props.storeList;
        let idx = 0
        let i = 0
        console.log(this.props.location.query.id);
        localStorage.setItem('id', this.props.location.query.id);
        console.log(this.props.storeList);
        for (i; i < this.props.storeList.length; i++) {
            if (arr[i].id == this.props.location.query.id) {
                idx = i;
                // localStorage.setItem('id', arr[i].id);
                localStorage.setItem('shopaddr', arr[i].shop_addr);
                localStorage.setItem('shopname', arr[i].shop_name);
                localStorage.setItem('shopimg', arr[i].shop_img);
                localStorage.setItem('shopmid', arr[i].shop_mtid);
                localStorage.setItem('shopcount', arr[i].shop_count);
                localStorage.setItem('shoptel', arr[i].shop_tel);
                localStorage.setItem('shopwm', arr[i].shop_announcementinfo);
                localStorage.setItem('shoplabel', arr[i].shop_label);
                localStorage.setItem('shopjw',arr[i].shop_jwd)
                localStorage.setItem('shopscount',arr[i].shop_starcount)
                localStorage.setItem('shopdistribution1',arr[i].shop_distribution1)
                localStorage.setItem('shopdistribution2',arr[i].shop_distribution2)
                localStorage.setItem('shopreduction1',arr[i].shop_reduction1)
                localStorage.setItem('shopreduction2',arr[i].shop_reduction2)
                localStorage.setItem('shopreduction3',arr[i].shop_reduction3)
                console.log(arr[i].shop_img)
                break;
            }

        }
        console.log(arr[i]);
        console.log(localStorage.getItem('id'));//106
        const fileList=[{
            uid:'-1',
            name:'shop.png',
            status:'done',
            url:'http://fourgoldhua.cn:3001'+localStorage.getItem('shopimg'),
            thumbUrl:'http://fourgoldhua.cn:3001'+localStorage.getItem('shopimg')
        }]

        // console.log(localStorage.getItem('shop'));
        return (
            <div>
                <Button onClick={()=>{history.back()}}>返回</Button>
                {/* <Link to='/store'>返回</Link> */}

                <Form {...layout} name="basic"
                    onFinish={this.onFinish}
                >
                    <Form.Item
                        label="店铺名称"
                        name="shopname"
                        initialValue={localStorage.getItem('shopname')}
                        rules={[{ required: true, message: '请输入店铺名称!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="店铺标签"
                        name="shoplabel"
                        initialValue={localStorage.getItem('shoplabel')}
                        rules={[{ required: true, message: '请输入店铺标签!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="店铺评分"
                        name="shopscount"
                        initialValue={localStorage.getItem('shopscount')}
                        rules={[{ required: true, message: '请输入店铺评分!' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="联系电话"
                        name="shoptel"
                        initialValue={localStorage.getItem('shoptel')}
                        rules={[{ required: true, message: '请输入店铺联系电话' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="店铺地址"
                        name="shopaddr"
                        initialValue={localStorage.getItem('shopaddr')}
                        rules={[{ required: true, message: '请输入店铺地址!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="外送方式"
                        name="shopwm"
                        initialValue={localStorage.getItem('shopwm')}
                        rules={[{ required: true, message: '请输入店铺外送方式!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="美团id"
                        name="shopmid"
                        initialValue={localStorage.getItem('shopmid')}
                        rules={[{ required: true, message: '请输入店铺美团id!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="经纬度"
                        name="shopjw"
                        initialValue={localStorage.getItem('shopjw')}
                        // rules={[{ required: true, message: '请输入店铺经纬度!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item label="店铺月销" name="shopcount"
                    initialValue={localStorage.getItem('shopcount')}
                    // rules={[{ required: true, message: '请输入店铺月销!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item label="起送费" name="shopdistribution1" 
                    initialValue={localStorage.getItem('shopdistribution1')}
                    // rules={[{ required: true, message: '请输入起送费!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item label="配送费" name="shopdistribution2" 
                    initialValue={localStorage.getItem('shopdistribution2')}
                    // rules={[{ required: true, message: '请输入配送费!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item label="满减活动1" name="shopreduction1"
                    initialValue={localStorage.getItem('shopreduction1')}>
                        <Input />
                    </Form.Item>
                    <Form.Item label="满减活动2" name="shopreduction2"
                    initialValue={localStorage.getItem('shopreduction2')}>
                        <Input />
                    </Form.Item>
                    <Form.Item label="满减活动3" name="shopreduction3"
                    initialValue={localStorage.getItem('shopreduction3')}>
                        <Input />
                    </Form.Item>
                    <Form.Item label="商家图片" name="shopimg">
                        <Upload defaultFileList={[...fileList]} listType="picture">
                            <Button icon={<UploadOutlined />}>Upload</Button>
                        </Upload>
                    </Form.Item>


                    {/* <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                        <Checkbox>Remember me</Checkbox>
                    </Form.Item> */}

                    <Form.Item {...tailLayout}>
                        <Button type="primary" htmlType="submit">
                            修改
                        </Button>
                    </Form.Item>

                </Form>

            </div>
        )
    }
}











