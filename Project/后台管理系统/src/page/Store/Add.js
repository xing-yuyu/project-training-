import React, { Component } from 'react'
import { Link } from 'umi'
import { connect } from 'dva';
import { Form, Input, Button,Upload,Checkbox } from 'antd';
import { message,Space } from 'antd';
import { UploadOutlined } from '@ant-design/icons';



const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};
const namespace = 'addstore';

const mapStateToProps = (state) => {
    return{
        a:123
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        // 展示
        onAddMount: (mess) => {
            // console.log(mess.shopimg.file.thumbUrl)
            dispatch({
                type: `${namespace}/queryAdd`,
                payload: mess
            })
        },

    };
};

@connect(mapStateToProps, mapDispatchToProps)

export class Add extends Component {
    onFinish =async values => {
        console.log(values);
        await this.props.onAddMount(values);
        await message.success('添加成功！',1);
        location.replace("http://localhost:8000/store")
    };

    render() {
        return (
            <div>
                <Link to='/store'>返回</Link>
                <Form {...layout} name="basic"
                    onFinish={this.onFinish}
                >
                    <Form.Item
                        label="店铺名称"
                        name="shopname"
                        // rules={[{ required: true, message: '请输入店铺名称!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="店铺标签"
                        name="shoplabel"
                        // rules={[{ required: true, message: '请输入店铺标签!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="店铺评分"
                        name="shopscount"
                        // rules={[{ required: true, message: '请输入店铺评分!' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="联系电话"
                        name="shoptel"
                        // rules={[{ required: true, message: '请输入店铺联系电话' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="店铺地址"
                        name="shopaddr"
                        // rules={[{ required: true, message: '请输入店铺地址!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="外送方式"
                        name="shopwm"
                        // rules={[{ required: true, message: '请输入店铺外送方式!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="经纬度"
                        name="shopmjw"
                        // rules={[{ required: true, message: '请输入店铺经纬度!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item label="店铺月销" name="shopcount"
                    // rules={[{ required: true, message: '请输入店铺月销!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item label="起送费" name="shopdistribution1" 
                    // rules={[{ required: true, message: '请输入起送费!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item label="配送费" name="shopdistribution2" 
                    // rules={[{ required: true, message: '请输入配送费!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item label="满减活动1" name="shopreduction1">
                        <Input />
                    </Form.Item>
                    <Form.Item label="满减活动2" name="shopreduction2">
                        <Input />
                    </Form.Item>
                    <Form.Item label="满减活动3" name="shopreduction3">
                        <Input />
                    </Form.Item>

                    <Form.Item label="商家图片" name="shopimg" 
                    // rules={[{ required: true, message: '请添加商铺图片!' }]}
                    >
                        <Upload listType="picture">
                            <Button icon={<UploadOutlined />}>Upload</Button>
                        </Upload>
                    </Form.Item>

                    {/* <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                        <Checkbox>Remember me</Checkbox>
                    </Form.Item> */}

                    <Form.Item {...tailLayout}>
                        <Button type="primary" htmlType="submit">
                            提交
                        </Button>
                    </Form.Item>

                </Form>
            </div>
        )
    }
}

export default Add
