import React, { Component } from 'react'
import { Link } from 'umi'
import { connect } from 'dva';
import { useContext, useState, useEffect, useRef } from 'react';
import { Table, Input, Button, Popconfirm, Form } from 'antd';


const namespace = 'getComment';

const mapStateToProps = (state) => {
    const commentList = state[namespace].data;
    return {
        commentList
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        // 展示评论
        onDidMount: () => {
            dispatch({
                type: `${namespace}/queryAdd`,
            })
        },
        //删除数据
        delComment: (value) => {
            console.log(value.id)
            dispatch({
                type: `${namespace}/queryDel`,
                payload:value.id
            })
        }
    }
}


@connect(mapStateToProps, mapDispatchToProps)

export default class Comment extends Component {

    constructor(props) {
        super(props);
        this.columns = [
            {
                title: '评论id',
                dataIndex: 'id',
                key: 'id',
            },
            {
                title: '用户名',
                dataIndex: 'user_name',
                key: 'user_name',
            },
            {
                title: '发布评论',
                dataIndex: 'text',
                key: 'text',
            },
            {
                title: '发布时间',
                dataIndex: 'date',
                key: 'date',
            },
            {
                title: 'Action',
                key: 'action',
                render: (e) => (
                    <Popconfirm title="确定删除该数据?" onConfirm={() => this.props.delComment(e)}>
                        <a>Delete</a>
                    </Popconfirm>
                ),
            },
        ];
    }

    componentDidMount() {
        this.props.onDidMount();
        setTimeout(() => {
            this.setState({ loading: false })
        }, 1500)
    };

    render() {
        console.log(this.props.commentList);
        const list = this.props.commentList
        return (
            <div>
                <Table columns={this.columns}
                    dataSource={this.props.commentList} />
            </div>
        )

    }
}



