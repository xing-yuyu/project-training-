import { Card, Col, Row } from 'antd';
import { connect } from 'dva';
import { Table, Input, Button, Popconfirm, Form } from 'antd';
import React, { Component } from 'react'

const namespace = 'helloStore';
const mapStateToProps = (state) => {
  const helloList = state[namespace].data;
  return {
    helloList
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    // 展示用户
    onDidMount: () => {
      dispatch({
        type: `${namespace}/querygetHello`,
      })
    },
  }
}



@connect(mapStateToProps, mapDispatchToProps)

export class HelloWorld extends Component {
  constructor(props) {
    super(props);
    this.state={
      data:{
        usernum:'loading',
        fabu:'loading',
        tonggou:'loading',
        weitongguo:'loading'
      }
    }
    this.style = {
      width: '270px',
      height: '140px',
      margin: '30px',
      marginRight: '7px',
      boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)',
      border: '1px solid #e8e8e8',
      textAlign: 'center'
    };
    this.style1 = {
      width: '95%',
      margin: '30px',
      boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2)',
      border: '1px solid #e8e8e8',
      textAlign: 'center'
    };
  }

  componentDidMount() {
    this.props.onDidMount();
    setTimeout(() => {
      this.setState({
        data:this.props.helloList[0]
      })
    }, 1500)
  };


  render() {
    console.log(this.props.helloList[0])
    this.wait
    return (
      <div>
        <Card style={this.style1}>
          <Card.Meta title="核心数据" />
        </Card>
        <Row>
          <Col>
            <Card style={this.style}>
              <Card.Meta title="累计用户" description={this.state.data.usernum} />
            </Card>
          </Col>
          <Col>
            <Card style={this.style}>
              <Card.Meta title="累计发布" description={this.state.data.fabu} />
            </Card>
          </Col>
          <Col>
            <Card style={this.style}>
              <Card.Meta title="审核通过" description={this.state.data.tongguo}  />
            </Card>
          </Col>
          <Col>
            <Card style={this.style}>
              <Card.Meta title="审核未通过" description={this.state.data.weitongguo}  />
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default HelloWorld




// export default () => {


//   return (
//     <div>


//     </div>

//   );
// }